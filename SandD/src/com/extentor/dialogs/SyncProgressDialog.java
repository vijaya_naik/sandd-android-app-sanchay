package com.extentor.dialogs;

import com.extentor.sfandroid.R;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class SyncProgressDialog extends Dialog
{
	Dialog d;
	private static SyncProgressDialog	mCustomProgressbar;
	private SyncProgressDialog			mProgressbar;
	private OnDismissListener			mOnDissmissListener;
	private TextView					textViewStatus, textViewStatus2;
	public TextView						textView1;
	private Button						buttonCancel;

	private SyncProgressDialog( Context context )
	{
		super( context );
		requestWindowFeature( Window.FEATURE_NO_TITLE );
		setContentView( R.layout.dialog_progressbar );
		this.getWindow().setBackgroundDrawableResource( android.R.color.transparent );
		textViewStatus = (TextView) findViewById( R.id.textViewStatus );
		textViewStatus2 = (TextView) findViewById( R.id.textViewStatus2 );
		textView1 = (TextView) findViewById( R.id.textView1 );
		buttonCancel = (Button) findViewById( R.id.buttonCancel );
	}

	public SyncProgressDialog( Context context, Boolean instance )
	{
		super( context );
		mProgressbar = new SyncProgressDialog( context );
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		if ( mOnDissmissListener != null )
		{
			mOnDissmissListener.onDismiss( this );
		}
	}

	public void SetStatusMessage( String status, String status2 )
	{
		textViewStatus.setText( status );
		textViewStatus2.setText( status2 );
	}

	public static SyncProgressDialog getProgressDialog()
	{
		return mCustomProgressbar;
	}

	public static void showProgressBar( Context context, boolean cancelable )
	{
		showProgressBar( context, cancelable, null );
	}

	public static void showProgressBar( Context context, boolean cancelable, String message )
	{
		if ( mCustomProgressbar != null && mCustomProgressbar.isShowing() )
		{
			mCustomProgressbar.cancel();
		}
		mCustomProgressbar = new SyncProgressDialog( context );
		mCustomProgressbar.setCancelable( cancelable );
		mCustomProgressbar.show();

	}

	public static void showProgressBar( Context context, OnDismissListener listener )
	{

		if ( mCustomProgressbar != null && mCustomProgressbar.isShowing() )
		{
			mCustomProgressbar.cancel();
		}
		mCustomProgressbar = new SyncProgressDialog( context );
		mCustomProgressbar.setListener( listener );
		mCustomProgressbar.setCancelable( Boolean.TRUE );
		mCustomProgressbar.show();
	}

	public static void hideProgressBar()
	{
		if ( mCustomProgressbar != null )
		{
			mCustomProgressbar.dismiss();
		}
	}

	private void setListener( OnDismissListener listener )
	{
		mOnDissmissListener = listener;

	}

	public static void showListViewBottomProgressBar( View view )
	{
		if ( mCustomProgressbar != null )
		{
			mCustomProgressbar.dismiss();
		}

		view.setVisibility( View.VISIBLE );
	}

	public static void hideListViewBottomProgressBar( View view )
	{
		if ( mCustomProgressbar != null )
		{
			mCustomProgressbar.dismiss();
		}

		view.setVisibility( View.GONE );
	}

	public void showProgress( Context context, boolean cancelable, String message )
	{
		if ( mProgressbar != null && mProgressbar.isShowing() )
		{
			mProgressbar.cancel();
		}
		mProgressbar.setCancelable( cancelable );
		mProgressbar.show();
	}

	public void showCancel( android.view.View.OnClickListener onClickListener )
	{
		buttonCancel.setVisibility( View.VISIBLE );
		buttonCancel.setOnClickListener( onClickListener );
	}
}