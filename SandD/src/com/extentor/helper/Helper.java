package com.extentor.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.sfandroid.SFApp;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Helper
{
	private static SFApp	m_Application;

	public Helper()
	{
		// TODO Auto-generated constructor stub
	}

	public static Application GetApplication()
	{
		return m_Application;
	}

	public static void SetApplication( SFApp application )
	{
		m_Application = application;
	}

	public static String GetStringValue( int resId )
	{
		return m_Application.getString( resId );
	}

	public static Resources GetResources()
	{
		return m_Application.getResources();
	}

	public static Resources GetResources( Context context )
	{
		if ( context instanceof Context )
		{
			return context.getResources();
		}
		return null;
	}

	public static Boolean CheckSharedPrefs( int resId )
	{
		if ( m_Application.sharedPrefs.contains( GetStringValue( resId ) ) )
			return true;
		return false;
	}

	public static void EditSharedPrefs( String key )
	{
		Editor editor = m_Application.sharedPrefs.edit();
		editor.putString( key, key );
		editor.commit();
	}

	public static void EditSharedPrefs( String key, Boolean value )
	{
		Editor editor = m_Application.sharedPrefs.edit();
		editor.putBoolean( key, value );
		editor.commit();
	}

	public static void EditSharedPrefs( String key, long value )
	{
		Editor editor = m_Application.sharedPrefs.edit();
		editor.putLong( key, value );
		editor.commit();
	}

	public static void EditSharedPrefs( String key, String value )
	{
		Editor editor = m_Application.sharedPrefs.edit();
		editor.putString( key, value );
		editor.commit();
	}

	public static void ClearSharedPrefs()
	{
		Editor editor = m_Application.sharedPrefs.edit();
		editor.clear();
		editor.commit();
	}

	public static String GetSharedPrefItem( String key )
	{
		return m_Application.sharedPrefs.getString( key, null );
	}

	public static long GetSharedPrefItemLong( String key )
	{
		return m_Application.sharedPrefs.getLong( key, -1 );
	}

	public static Boolean GetSharedPrefItemBoolean( String key )
	{
		return m_Application.sharedPrefs.getBoolean( key, false );
	}

	public static String Capitalize( String input )
	{
		return input.substring( 0, 1 ).toUpperCase() + input.substring( 1 );
	}

	public static String ProcessKey( String string )
	{
		String temp = string.replace( "__c", "" ).replace( "_", "" );
		String[] arr = temp.split( "(?<=[a-z])(?=[A-Z])" );
		StringBuilder builder = new StringBuilder();
		for ( int i = 0; i < arr.length; i++ )
		{
			builder.append( Capitalize( arr[i] ) + " " );
		}
		temp = builder.toString().trim();
		return temp;
	}

	public static boolean isInternetAvailable( Context appContext, Activity activity, Boolean showDialog )
	{
		ConnectivityManager connectivity = (ConnectivityManager) appContext.getSystemService( Context.CONNECTIVITY_SERVICE );
		if ( connectivity != null )
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if ( info != null )
				for ( int i = 0; i < info.length; i++ )
					if ( info[i].getState() == NetworkInfo.State.CONNECTED )
					{
						return true;
					}

		}
		if ( showDialog )
		{
			AlertDialog.Builder alertDialog = new AlertDialog.Builder( activity );
			alertDialog.setTitle( "Alert!" );
			alertDialog.setMessage( "No Internet Connection" );
			Dialog alert = alertDialog.create();
			alert.show();
		}
		return false;
	}

	public static void SetKeyValue( JSONObject object, String key, String value )
	{
		try
		{
			object.put( key, value );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public static String GetDate( String dateString )
	{
		SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSSZ" );
		formatter.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
		Date value = null;
		try
		{
			value = formatter.parse( dateString );
		}
		catch ( ParseException e )
		{
			e.printStackTrace();
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat( "MM/dd/yyyy hh:mm aa Z" );
		dateFormatter.setTimeZone( TimeZone.getDefault() );
		String dt = dateFormatter.format( value );

		return dt;
	}
}