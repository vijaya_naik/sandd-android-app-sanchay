package com.extentor.helper;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.extentor.sfandroid.SFApp;
import com.salesforce.androidsdk.smartstore.store.IndexSpec;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;
import com.salesforce.androidsdk.smartstore.store.SmartStore.Type;

public class SoupHelper
{
	public String	soupName;
	public Object	data;

	public SoupHelper( String soupName, Object data )
	{
		this.soupName = soupName;
		this.data = data;
	}

	public static IndexSpec[] CreateIndexSpecs( JSONArray array )
	{
		if ( array.length() > 0 )
		{
			IndexSpec[] indexSpec = new IndexSpec[array.length()];
			for ( int i = 0; i < array.length(); i++ )
			{
				try
				{
					String path = array.getString( i );
					indexSpec[i] = new IndexSpec( path, Type.string );
				}
				catch ( JSONException e )
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					indexSpec = null;
				}
			}

			return indexSpec;
		}

		return null;
	}

	public static void RegisterSoup( String soupName, JSONArray data ) throws Exception
	{
		SmartStore smartStore = SFApp.GetSmartStore();
		IndexSpec[] indexSpec = CreateIndexSpecs( data );
		try
		{
			smartStore.registerSoup( soupName, indexSpec );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
			Log.e( "SOUP REGISTER ERROR", soupName );
			throw e;
		}

		Log.d( "SOUP REGISTERED", soupName );
	}

	public static void InsertData( String soupName, JSONObject data ) throws JSONException
	{
		SmartStore smartStore = SFApp.GetSmartStore();
		try
		{
			smartStore.upsert( soupName, data );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
	}

	public static List<String> GetAllSoupNames()
	{
		return SFApp.GetSmartStore().getAllSoupNames();
	}

	public static ArrayList<String> GetSoupFieldNamesList( String soupName )
	{
		IndexSpec[] indexSpecs = SFApp.GetSmartStore().getSoupIndexSpecs( soupName );
		ArrayList<String> retVal = new ArrayList<String>();
		for ( int i = 0; i < indexSpecs.length; i++ )
		{
			if( indexSpecs[i].path.contains( "GeocodeAccuracy" ) )
				continue;
			retVal.add( indexSpecs[i].path );
		}
		return retVal;
	}

	public static JSONArray GetSoupFieldNames( String soupName )
	{
		JSONArray retArr = new JSONArray();
		try
		{
			IndexSpec[] indexSpecs = SFApp.GetSmartStore().getSoupIndexSpecs( soupName );

			for ( int i = 0; i < indexSpecs.length; i++ )
			{
				retArr.put( indexSpecs[i].path );
			}
		}
		catch ( Exception e )
		{
			// TODO: handle exception
			e.printStackTrace();
		}
		return retArr;
	}

	public static String GetSelectQuery( String soupName )
	{
		StringBuilder retVal = new StringBuilder();

		retVal.append( "SELECT " );

		SmartStore smartStore = SFApp.GetSmartStore();
		IndexSpec[] indexSpecs = smartStore.getSoupIndexSpecs( soupName );

		for ( int i = 0; i < indexSpecs.length; i++ )
		{
			IndexSpec indexSpec = indexSpecs[i];
			retVal.append( "{" + soupName + ":" + indexSpec.path + "}" );
			if ( i == indexSpecs.length - 1 )
			{
				retVal.append( " FROM {" + soupName + "}" );
				break;
			}
			retVal.append( ", " );
		}

		return retVal.toString();
	}

	public static JSONArray RunSmartQuery( QuerySpec querySpec, int pageIndex )
	{
		return null;
	}
}
