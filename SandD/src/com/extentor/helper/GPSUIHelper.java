package com.extentor.helper;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.view.View;
import android.view.View.OnClickListener;
import com.extentor.dialogs.SyncProgressDialog;
import com.extentor.interfaces.IGPSUI;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.activity.AddOrderActivity;
import com.extentor.sfobjects.ISFObject;

public class GPSUIHelper implements IGPSUI
{
	private Activity			m_activity;
	private ISFObject			m_sfObject;
	private SyncProgressDialog	m_progress;

	public GPSUIHelper()
	{
		m_activity = null;
		m_sfObject = null;
		m_progress = null;
	}

	public GPSUIHelper( Activity activity )
	{
		this();
		m_activity = activity;
	}

	public GPSUIHelper( Activity activity, ISFObject sfObject )
	{
		this( activity );
		m_sfObject = sfObject;
	}

	@Override
	public void onStart()
	{
		SyncProgressDialog.showProgressBar( m_activity, false );
		m_progress = SyncProgressDialog.getProgressDialog();
		m_progress.textView1.setText( "Fetching location..." );
		m_progress.showCancel( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				GPSHelper gpsHelper = GPSHelper.getInstance();
				gpsHelper.cancel();
				SyncProgressDialog.hideProgressBar();
			}
		} );
	}

	@Override
	public void onUpdate( Location location )
	{
		SyncProgressDialog.hideProgressBar();
		Intent intentAddOrder = new Intent( m_activity, AddOrderActivity.class );

		intentAddOrder.putExtra( m_activity.getString( R.string.ORDER_LOCATION ), location );
		if( m_sfObject != null )
		{
			intentAddOrder.putExtra( "dist", m_sfObject.getData().toString() );
		}

		m_activity.startActivity( intentAddOrder );
		m_activity.overridePendingTransition( R.anim.navigate_in, R.anim.navigate_out );
	}

	@Override
	public void onSearching( String satInfo )
	{
		m_progress.SetStatusMessage( "Searching...", satInfo );
	}

	@Override
	public void onFirstFix()
	{
		m_progress.SetStatusMessage( "", "" );
	}

	@Override
	public void onStop()
	{
		m_progress.textView1.setText( "No GPS" );
	}
}
