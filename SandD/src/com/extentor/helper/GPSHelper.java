package com.extentor.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.extentor.interfaces.IGPSUI;
import com.extentor.sfobjects.ISFObject;

public class GPSHelper
{
	private Context				m_Context				= null;
	private LocationListener	m_LocationListenerGPS	= null;
	private LocationListener	m_LocationListenerNW	= null;
	private GpsStatus.Listener	m_GPSStatusListener		= null;
	private LocationManager		m_LocationManager		= null;
	public static GPSHelper		m_instance				= null;
	private IGPSUI				m_gps					= null;

	private GPSHelper( Context context, IGPSUI gps )
	{
		m_Context = context;
		m_gps = gps;
		m_LocationManager = (LocationManager) m_Context.getSystemService( Context.LOCATION_SERVICE );
	}

	public static GPSHelper getInstance()
	{
		return m_instance;
	}

	public static GPSHelper getInstance( Context context )
	{
		if ( m_instance == null )
		{
			m_instance = new GPSHelper( context, null );
		}
		else if ( !m_instance.m_Context.equals( context ) )
		{
			// check for context change
			m_instance = new GPSHelper( context, m_instance.m_gps );
		}
		m_instance.m_gps = new GPSUIHelper( (Activity) context );
		return m_instance;
	}

	public static GPSHelper getInstance( Context context, ISFObject obj )
	{
		m_instance = getInstance( context );
		m_instance.m_gps = new GPSUIHelper( (Activity) context, obj );
		return m_instance;
	}

	public boolean checkGPS()
	{
		boolean retVal = m_LocationManager.isProviderEnabled( LocationManager.GPS_PROVIDER );
		if ( !retVal )
		{
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( m_Context );
			alertDialogBuilder.setMessage( "Kindly enable GPS on your device." ).setCancelable( false ).setPositiveButton( "Settings", new DialogInterface.OnClickListener()
			{
				public void onClick( DialogInterface dialog, int id )
				{
					Intent callGPSSettingIntent = new Intent( android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS );
					m_Context.startActivity( callGPSSettingIntent );
				}
			} );
			alertDialogBuilder.setNegativeButton( "Cancel", new DialogInterface.OnClickListener()
			{
				public void onClick( DialogInterface dialog, int id )
				{
					dialog.cancel();
				}
			} );
			AlertDialog alert = alertDialogBuilder.create();
			alert.show();
		}
		return retVal;
	}

	public void getGPS()
	{
		if ( m_gps != null )
			m_gps.onStart();

		m_GPSStatusListener = new GpsStatus.Listener()
		{
			@Override
			public void onGpsStatusChanged( int event )
			{
				switch ( event )
				{
					case GpsStatus.GPS_EVENT_STARTED :
						System.out.println( "TAG - GPS searching: " );
						m_gps.onSearching( "" );
						break;
					case GpsStatus.GPS_EVENT_STOPPED :
						System.out.println( "TAG - GPS Stopped" );
						m_gps.onStop();
						break;
					case GpsStatus.GPS_EVENT_FIRST_FIX :
						m_gps.onFirstFix();
						Location gpslocation = m_LocationManager.getLastKnownLocation( LocationManager.GPS_PROVIDER );
						if ( gpslocation != null )
						{
							System.out.println( "GPS Info:" + gpslocation.getLatitude() + ":" + gpslocation.getLongitude() );
							m_LocationManager.removeGpsStatusListener( m_GPSStatusListener );
						}
						break;
					case GpsStatus.GPS_EVENT_SATELLITE_STATUS :
						final GpsStatus gs = m_LocationManager.getGpsStatus( null );
						int satsInView = 0,
						satsInUse = 0;
						for ( GpsSatellite satellite : gs.getSatellites() )
						{
							satsInView++;
							if ( satellite.getSnr() > 0 || satellite.usedInFix() )
							{
								satsInUse++;
							}
						}
						m_gps.onSearching( String.format( "Using %d satellites of %d", satsInUse, satsInView ) );
						break;
				}
			}
		};

		m_LocationListenerGPS = new LocationListener()
		{
			public void onLocationChanged( Location location )
			{
				cancel();
				if ( m_gps != null )
					m_gps.onUpdate( location );
			}

			public void onStatusChanged( String provider, int status, Bundle extras )
			{
			}

			public void onProviderEnabled( String provider )
			{
			}

			public void onProviderDisabled( String provider )
			{
			}
		};
		
		m_LocationListenerNW = new LocationListener()
		{
			@Override
			public void onStatusChanged( String provider, int status, Bundle extras )
			{
			}
			
			@Override
			public void onProviderEnabled( String provider )
			{
			}
			
			@Override
			public void onProviderDisabled( String provider )
			{
			}
			
			@Override
			public void onLocationChanged( Location location )
			{			
				cancel();
				if ( m_gps != null )
					m_gps.onUpdate( location );
			}
		};

		m_LocationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, m_LocationListenerGPS );
		m_LocationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 0, 0, m_LocationListenerNW );
		m_LocationManager.addGpsStatusListener( m_GPSStatusListener );
	}

	public void cancel()
	{
		m_LocationManager.removeGpsStatusListener( m_GPSStatusListener );
		m_LocationManager.removeUpdates( m_LocationListenerGPS );
		m_LocationManager.removeUpdates( m_LocationListenerNW );
	}
}
