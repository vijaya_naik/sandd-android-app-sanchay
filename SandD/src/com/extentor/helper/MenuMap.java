package com.extentor.helper;

public class MenuMap
{
	public class MenuOrder
	{
		private String	m_menuName;
		private int		m_order;
		private Class<?>	m_classActivity;

		public MenuOrder( String menuName, Class<?> classActivity )
		{
			this.m_menuName = menuName;
			this.m_order = 0;
			this.m_classActivity = classActivity;
		}
		
		public String getMenuName()
		{
			return m_menuName;
		}
		
		public int getMenuOrder()
		{
			return m_order;
		}
		
		public void setMenuOrder(String menuName, int menuOrder)
		{
			
		}
		
		public Class<?> getActivityClass()
		{
			return m_classActivity;
		}
	}

	public MenuMap()
	{
		// TODO Auto-generated constructor stub
	}
}
