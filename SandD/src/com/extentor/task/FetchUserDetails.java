package com.extentor.task;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.extentor.helper.Helper;
import com.extentor.interfaces.IDataFetch;
import com.extentor.interfaces.IStrategy;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

public class FetchUserDetails implements IStrategy
{
	private RestClient	m_client;
	private IDataFetch	m_delegate;
	private String		m_api;
	private String		m_query;

	public FetchUserDetails( RestClient client, String api, String query, IDataFetch delegate )
	{
		this.m_client = client;
		this.m_api = api;
		this.m_query = query;
		this.m_delegate = delegate;
	}

	@Override
	public void doOperation()
	{
		new MyAsyncTask().execute( m_api, m_query );
	}

	private class MyAsyncTask extends AsyncTask<String, Void, Exception>
	{
		private RestResponse	response	= null;

		@Override
		protected Exception doInBackground( String... params )
		{
			Exception excp = null;
			try
			{
				RestRequest request = RestRequest.getRequestForQuery( params[0], params[1] );
				response = m_client.sendSync( request );
				
				JSONObject userInfo = response.asJSONObject();
				JSONArray userInfoRecords = userInfo.getJSONArray( "records" );
				userInfo = userInfoRecords.getJSONObject( 0 );

				String name = userInfo.getString( "Name" );
				Helper.EditSharedPrefs( "name", name );
			}
			catch ( Exception e )
			{
				e.printStackTrace();
				excp = e;
			}
			return excp;
		}

		@Override
		protected void onPostExecute( Exception result )
		{
			if ( m_delegate != null )
			{
				if ( result != null )
				{
					m_delegate.onDataFetchError( result );
				}
				else
				{
					m_delegate.onDataFetchSuccess();
				}
			}
		}
	}
}
