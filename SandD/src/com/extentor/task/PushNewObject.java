package com.extentor.task;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.extentor.helper.Helper;
import com.extentor.interfaces.IDataFetch;
import com.extentor.interfaces.IStrategy;
import com.extentor.sfandroid.R;
import com.salesforce.androidsdk.app.SalesforceSDKManager;

public class PushNewObject implements IStrategy
{
	private IDataFetch	m_delegate;
	private String		m_endPoint;
	private String		m_Order;
	private String		m_OauthToken;
	private Context		m_context;

	public PushNewObject( Context context, IDataFetch delegate )
	{
		m_endPoint = SalesforceSDKManager.getInstance().getUserAccountManager().getCurrentUser().getInstanceServer() + context.getString( R.string.pushNewObject_EndPoint );
		m_delegate = delegate;
		m_context = context;
		m_Order = Helper.GetSharedPrefItem( context.getString( R.string.NEW_ORDER ) );		
//		[{"orderLineItemRecord":[{"Quantity__c":"10","Product_Price__c":"a08O000000IpF7BIAV","attributes":{"type":"Order_Line_item__c"},"Packing_Size__c":"PAK"},{"Quantity__c":"20","Product_Price__c":"a08O000000IpF6pIAF","attributes":{"type":"Order_Line_item__c"},"Packing_Size__c":"PAK"}],"orderRecord":{"Distributor__c":"001O000000kBjZzIAK","attributes":{"type":"Order__c"}}},{"orderLineItemRecord":[{"Quantity__c":"30","Product_Price__c":"a08O000000IpF7BIAV","attributes":{"type":"Order_Line_item__c"},"Packing_Size__c":"PAK"}],"orderRecord":{"Distributor__c":"001O000000kBjZzIAK","attributes":{"type":"Order__c"}}},{"orderLineItemRecord":[{"Quantity__c":"2","Product_Price__c":"a08O000000IpF7BIAV","attributes":{"type":"Order_Line_item__c"},"Packing_Size__c":"PAK"}],"orderRecord":{"Distributor__c":"001O000000kBjZzIAK","attributes":{"type":"Order__c"}}},{"orderLineItemRecord":[{"Quantity__c":"15","Product_Price__c":"a08O000000IpF6FIAV","attributes":{"type":"Order_Line_item__c"},"Packing_Size__c":"PAK"}],"orderRecord":{"Distributor__c":"001O000000kBjhbIAC","attributes":{"type":"Order__c"}}}]
		m_OauthToken = SalesforceSDKManager.getInstance().getUserAccountManager().getCurrentUser().getAuthToken();
	}

	@Override
	public void doOperation()
	{
		if ( this.m_Order == null )
		{
			m_delegate.onDataFetchSuccess();
			return;
		}
		if( this.m_Order.length() == 0 )
		{
			m_delegate.onDataFetchSuccess();
			return;
		}
		
		//process m_Order before sending it
		try
		{
			JSONArray arr = new JSONArray( m_Order );
			int count = arr.length();
			if( count == 0 )
			{
				m_delegate.onDataFetchSuccess();
				return;
			}
			for ( int i = 0; i < count; i++ )
			{
				JSONObject obj = arr.getJSONObject( i );
				obj.remove( m_context.getString( R.string.ORDER_DISPLAY_DATA ) );
			}
			m_Order = arr.toString();
		}
		catch ( JSONException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Response.Listener<String> responseListener = new Response.Listener<String>()
		{
			@Override
			public void onResponse( String arg0 )
			{
				Helper.EditSharedPrefs( PushNewObject.this.m_context.getString( R.string.NEW_ORDER ), "" );
				m_delegate.onDataFetchSuccess();
			}
		};
		
		Response.ErrorListener errorListener = new Response.ErrorListener()
		{

			@Override
			public void onErrorResponse( VolleyError arg0 )
			{
				Exception exc = new Exception( arg0.getMessage() );
				m_delegate.onDataFetchError( exc );
			}
		};

		StringRequest postRequest = new StringRequest( Request.Method.POST, this.m_endPoint, responseListener, errorListener )
		{
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError
			{
				Map<String, String> params = new HashMap<>();
				// the header parameters:
				params.put( "Authorization", "Bearer " + m_OauthToken );
				return params;
			}

			@Override
			public byte[] getBody() throws AuthFailureError
			{
				return m_Order.getBytes();
			}

			@Override
			public String getBodyContentType()
			{
				return "application/json";
			}
		};

		Volley.newRequestQueue( this.m_context ).add( postRequest );
		
		/*
		Volley.newRequestQueue( this.m_context, new HurlStack()
		{
			@Override
			protected HttpURLConnection createConnection( URL url ) throws IOException
			{
				HttpURLConnection connection = super.createConnection( url );
				connection.setRequestProperty( "Accept-Encoding", "" );
				return connection;
			}
		} ).add( postRequest );*/
	}
}
