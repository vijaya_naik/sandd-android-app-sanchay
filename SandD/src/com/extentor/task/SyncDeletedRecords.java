package com.extentor.task;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.extentor.helper.SoupHelper;
import com.extentor.interfaces.IDataFetch;
import com.extentor.interfaces.IStrategy;
import com.extentor.sfandroid.SFApp;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

public class SyncDeletedRecords implements IStrategy
{
	private RestClient	m_client;
	private IDataFetch	m_delegate;
	private String		m_endPoint;
	
	public SyncDeletedRecords( RestClient client, String endPoint, IDataFetch delegate )
	{
		this.m_client = client;
		this.m_endPoint = endPoint;
		this.m_delegate = delegate;
	}
	
	@Override
	public void doOperation()
	{
		new MyAsyncTask().execute( this.m_endPoint );
	}
	
	private class MyAsyncTask extends AsyncTask<String, Void, Exception>
	{
		private RestResponse	response	= null;

		@Override
		protected Exception doInBackground( String... params )
		{
			List<String> m_Soups = SoupHelper.GetAllSoupNames();
			m_Soups.remove( "syncs_soup" );
			Exception excp = null;
			for( String soupName : m_Soups )
			{
				final DateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				Date now = new Date();
				Date previous = new Date( now.getTime() - 29*24*3600*1000L );
				String dateStart = null, dateEnd = null;
				try
				{
					dateStart = URLEncoder.encode( TIMESTAMP_FORMAT.format( previous ), "UTF-8" );
					dateEnd = URLEncoder.encode( TIMESTAMP_FORMAT.format( now ), "UTF-8" );
				}
				catch ( UnsupportedEncodingException e1 )
				{
					e1.printStackTrace();
					excp = e1;
					break;
				}
				String url = String.format( params[0], soupName ).concat( String.format( "?start=%s&end=%s", dateStart, dateEnd ) );
				RestRequest restRequest = new RestRequest( RestRequest.RestMethod.GET, url, null );
				try
				{
					response = m_client.sendSync( restRequest );
					deleteRecords( soupName, response );
				}
				catch ( Exception e )
				{
					e.printStackTrace();
					excp = e;
					break;
				}
			}
			return excp;
		}

		@Override
		protected void onPostExecute( Exception result )
		{
			if ( m_delegate != null )
			{
				if ( result != null )
				{
					m_delegate.onDataFetchError( result );
				}
				else
				{
					m_delegate.onDataFetchSuccess();
				}
			}
		}
	}

	public void deleteRecords( String soupName, RestResponse result ) throws Exception
	{
		JSONObject obj = ( (RestResponse) result ).asJSONObject();
		JSONArray deletedRecords = obj.getJSONArray( "deletedRecords" );
		int length = deletedRecords.length();
		if( length == 0 ) return;
		ArrayList<Long> ids = new ArrayList<>();
		SmartStore smartStore = SFApp.GetSmartStore();
		for ( int i = 0; i < length; i++ )
		{
			JSONObject recordToDelete = deletedRecords.getJSONObject( i );
			String strId = recordToDelete.getString( "id" );
			Long longId = smartStore.lookupSoupEntryId( soupName, "Id", strId );
			ids.add( longId );
		}
		
		smartStore.delete( soupName, (Long[]) ids.toArray(new Long[ids.size()]) );
	}
}
