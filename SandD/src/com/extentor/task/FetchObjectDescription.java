package com.extentor.task;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.extentor.helper.Helper;
import com.extentor.helper.SoupHelper;
import com.extentor.interfaces.IDataFetch;
import com.extentor.interfaces.IStrategy;
import com.extentor.sfandroid.activity.Home;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

public class FetchObjectDescription implements IStrategy
{
	private RestClient		m_client;
	private IDataFetch		m_delegate;
	private String			m_endPoint;
	private List<String>	soups;
	JSONObject				objectLayouts	= new JSONObject();

	public FetchObjectDescription( RestClient client, String endPoint, IDataFetch delegate )
	{
		this.m_client = client;
		this.m_delegate = delegate;
		this.m_endPoint = endPoint;
	}

	@Override
	public void doOperation()
	{
		if ( Helper.GetSharedPrefItem( "objectlayouts" ) != null )
		{
			m_delegate.onDataFetchSuccess();
			return;
		}
		( (Home) FetchObjectDescription.this.m_delegate ).UpdateProgress( "Sync running for layouts...", "");
		soups = SoupHelper.GetAllSoupNames();
		new MyAsyncTask().execute( this.m_endPoint );
	}

	private class MyAsyncTask extends AsyncTask<String, String, Exception>
	{
		private RestResponse	response	= null;

		@Override
		protected Exception doInBackground( String... params )
		{
			Exception ex = null;
			while ( !soups.isEmpty() )
			{
				String soup = soups.remove( 0 );
				String endPoint = String.format( params[0], soup );

				FetchObjectLayout( soup, null, endPoint );
			}
			return ex;
		}

		private void FetchObjectLayout( String soup, String layoutName, String endPoint )
		{
			RestRequest restRequest = new RestRequest( RestRequest.RestMethod.GET, endPoint, null );
			try
			{
				response = m_client.sendSync( restRequest );
				JSONObject object = response.asJSONObject();

				if ( object.has( "layouts" ) && object.isNull( "layouts" ) )
				{
					JSONArray recordTypeMappings = object.getJSONArray( "recordTypeMappings" );
					int count = recordTypeMappings.length();
					for ( int i = 0; i < count; i++ )
					{
						JSONObject obj = recordTypeMappings.getJSONObject( i );
						layoutName = obj.getString( "name" );
						String url = obj.getJSONObject( "urls" ).getString( "layout" );
						FetchObjectLayout( soup, layoutName, url );
						this.publishProgress( soup, layoutName );
					}
				}
				else
				{
					if ( object.has( "recordTypeMappings" ) )
					{
						JSONArray recordTypeMappings = object.getJSONArray( "recordTypeMappings" );
						layoutName = recordTypeMappings.getJSONObject( 0 ).getString( "name" );
						if( object.has( "layouts" ) )
						{
							JSONArray layouts = object.getJSONArray( "layouts" );
							for( int i = 0; i < layouts.length(); i++ )
							{
								JSONObject obj = layouts.getJSONObject( i );
								SaveObjectLayout( soup, layoutName, obj );
							}
						}
					}
					else
					{
						SaveObjectLayout( soup, layoutName, object );
						this.publishProgress( soup, layoutName );
					}
				}
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
		}

		private void SaveObjectLayout( String soup, String layoutName, JSONObject object )
		{
			try
			{
				JSONObject fieldNames = new JSONObject();

				JSONArray detailLayoutSections = object.getJSONArray( "detailLayoutSections" );
				int count = detailLayoutSections.length();

				for ( int i = 0; i < count; i++ )
				{
					JSONArray layoutRows = detailLayoutSections.getJSONObject( i ).getJSONArray( "layoutRows" );
					int count2 = layoutRows.length();
					for ( int j = 0; j < count2; j++ )
					{
						JSONObject _obj_ = layoutRows.getJSONObject( j );
						JSONArray layoutItems = _obj_.getJSONArray( "layoutItems" );
						int count3 = layoutItems.length();
						for ( int k = 0; k < count3; k++ )
						{
							JSONObject item = layoutItems.getJSONObject( k );
							JSONArray layoutComponents = item.getJSONArray( "layoutComponents" );
							int count4 = layoutComponents.length();
							for ( int l = 0; l < count4; l++ )
							{
								JSONObject layoutComponent = layoutComponents.getJSONObject( l );
								if ( layoutComponent.has( "details" ) )
								{
									JSONObject details = layoutComponent.getJSONObject( "details" );
									String label = details.getString( "label" );
									String name = details.getString( "name" );
									fieldNames.put( name, label );
								}
							}
						}
					}
				}
				if ( objectLayouts.has( soup ) )
				{
					JSONObject obj = objectLayouts.getJSONObject( soup );
					obj.put( layoutName, fieldNames );
				}
				else
				{	
					JSONObject obj = new JSONObject();
					obj.put( layoutName, fieldNames  );
					objectLayouts.put( soup, obj );
				}
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
		}

		@Override
		protected void onProgressUpdate( String... values )
		{
			( (Home) FetchObjectDescription.this.m_delegate ).UpdateProgress( "Sync running for layout:", Helper.ProcessKey( values[0] ) + " : [" + values[1] + "]");
		}

		@Override
		protected void onPostExecute( Exception result )
		{
			if ( m_delegate != null )
			{
				if ( result != null )
				{
					m_delegate.onDataFetchError( result );
				}
				else
				{
					Helper.EditSharedPrefs( "objectlayouts", objectLayouts.toString() );
					m_delegate.onDataFetchSuccess();
				}
			}
		}
	}
}
