package com.extentor.task;

import java.util.ArrayDeque;
import java.util.Queue;

import android.os.Handler;
import android.os.Message;

public class TaskQueue
{
	private Queue<DataTask>	tasks	= new ArrayDeque<DataTask>();
	private static TaskQueue m_taskQueue;
	
	private TaskQueue()
	{
		
	}
	
	public static TaskQueue getInstance()
	{
		if( m_taskQueue == null )
		{
			m_taskQueue = new TaskQueue();
		}
		return m_taskQueue;
	}
	
	public int GetTaskCount()
	{
		return tasks.size();
	}
	
	public void Clear()
	{
		tasks.clear();
	}

	public void AddTask( DataTask task )
	{
		tasks.add( task );
	}

	private Handler	handler	= new Handler()
	{
		@Override
		public void handleMessage( Message msg )
		{
			TaskQueue.this.Execute();
		}
	};

	private void Execute()
	{
		final DataTask task = tasks.poll();
		if ( task == null )
		{
			return;
		}
		task.Run();
	}

	public void RunNext()
	{
		handler.sendEmptyMessage( 0 );
	}
}
