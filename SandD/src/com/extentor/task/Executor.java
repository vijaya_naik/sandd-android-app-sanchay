package com.extentor.task;

import com.extentor.interfaces.IStrategy;

public class Executor extends DataTask
{
	private IStrategy m_strategy;
	
	public Executor(IStrategy strategy)
	{
		m_strategy = strategy;
	}

	@Override
	protected void Run()
	{
		m_strategy.doOperation();
	}
}
