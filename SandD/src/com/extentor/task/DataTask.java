package com.extentor.task;

public abstract class DataTask
{
	private TaskQueue	taskQueue;

	abstract protected void Run();

	protected void Complete()
	{
		taskQueue.RunNext();
	}
}
