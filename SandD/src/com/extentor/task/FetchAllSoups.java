package com.extentor.task;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.extentor.helper.SoupHelper;
import com.extentor.interfaces.IDataFetch;
import com.extentor.interfaces.IStrategy;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

public class FetchAllSoups implements IStrategy
{
	private RestClient	m_client;
	private IDataFetch	m_delegate;
	private String		m_endPoint;

	public FetchAllSoups( RestClient client, String endPoint, IDataFetch delegate )
	{
		this.m_client = client;
		this.m_endPoint = endPoint;
		this.m_delegate = delegate;
	}

	@Override
	public void doOperation()
	{
		new MyAsyncTask().execute( this.m_endPoint );
	}

	private class MyAsyncTask extends AsyncTask<String, Void, Exception>
	{
		private RestResponse	response	= null;

		@Override
		protected Exception doInBackground( String... params )
		{
			RestRequest restRequest = new RestRequest( RestRequest.RestMethod.GET, params[0], null );
			Exception excp = null;
			try
			{
				response = m_client.sendSync( restRequest );
				parseAllSoupNames( response );
			}
			catch ( Exception e )
			{
				e.printStackTrace();
				excp = e;
			}
			return excp;
		}

		@Override
		protected void onPostExecute( Exception result )
		{
			if ( m_delegate != null )
			{
				if ( result != null )
				{
					m_delegate.onDataFetchError( result );
				}
				else
				{
					m_delegate.onDataFetchSuccess();
				}
			}
		}
	}

	public void parseAllSoupNames( RestResponse result ) throws Exception
	{
		JSONArray array = ( (RestResponse) result ).asJSONArray();
		for ( int i = 0; i < array.length(); i++ )
		{
			JSONObject obj = array.getJSONObject( i );
			String soupName = obj.getString( "objectName" );
			JSONArray fields = obj.getJSONArray( "fields" );
			SoupHelper.RegisterSoup( soupName, fields );
		}
	}
}
