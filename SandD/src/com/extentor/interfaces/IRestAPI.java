package com.extentor.interfaces;

import com.salesforce.androidsdk.rest.RestResponse;

public interface IRestAPI
{
	public void onRequestStart();

	public void onSuccess( RestResponse result );

	public void onError( Exception exception );
}