package com.extentor.interfaces;

public interface IDataBehavior
{
	public Object ProcessData( Object data );
}
