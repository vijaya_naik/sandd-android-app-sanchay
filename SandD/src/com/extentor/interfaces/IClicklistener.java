package com.extentor.interfaces;

import android.view.View;

/**
 * Created by Ankur on 8/3/2015.
 */
public interface IClicklistener {
    public void onClick(View view, int position);

    public void onLongClick(View view, int position);
}
