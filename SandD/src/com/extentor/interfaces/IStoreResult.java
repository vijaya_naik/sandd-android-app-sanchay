package com.extentor.interfaces;

import org.json.JSONArray;

public interface IStoreResult
{
	public void onSuccess();

	public void onSuccess( JSONArray result );

	public void onError( Exception exception );
}
