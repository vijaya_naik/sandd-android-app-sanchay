package com.extentor.interfaces;

import org.json.JSONObject;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;

public interface IInputField
{
	public abstract void Draw( ViewGroup parent );
	public void Bind( JSONObject object, String key );
	public String GetValue();
	public abstract void SetValue();
	public abstract void SetLayoutParam( LayoutParams params );
}