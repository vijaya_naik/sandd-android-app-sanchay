package com.extentor.interfaces;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IStore
{
	public void onStart();

	public void onComplete();

	public void onProgress( JSONObject object );

	public void onComplete( JSONArray result );

	public void onError( Exception e );
}