package com.extentor.interfaces;

public interface IDataFetch
{
	public void onDataFetchStart();

	public void onDataFetchSuccess();

	public void onDataFetchError( Exception exception );
}