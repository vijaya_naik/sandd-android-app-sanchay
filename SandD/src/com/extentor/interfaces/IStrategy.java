package com.extentor.interfaces;

public interface IStrategy
{
	public void doOperation();
}
