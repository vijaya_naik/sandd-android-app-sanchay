package com.extentor.interfaces;

import android.view.View;

/**
 * Created by User on 8/3/2015.
 */
public interface IFragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
}
