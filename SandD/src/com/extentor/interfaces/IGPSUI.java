package com.extentor.interfaces;

import android.location.Location;

public interface IGPSUI
{	
	public void onStart();
	public void onSearching( String satInfo );
	public void onFirstFix();
	public void onStop();
	public void onUpdate( Location location );
}
