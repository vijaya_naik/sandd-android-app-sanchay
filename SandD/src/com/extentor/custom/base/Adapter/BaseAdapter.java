package com.extentor.custom.base.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import com.extentor.sfandroid.R;

import com.extentor.custom.base.Model.BaseModel;

/**
 * Created by Ankur on 7/30/2015.
 */
public class BaseAdapter extends RecyclerView.Adapter<BaseAdapter.MyViewHolder>
{
	List<BaseModel>			data				= Collections.emptyList();
	private LayoutInflater	inflater;
	Boolean					m_showDeleteButton	= false;

	public BaseAdapter( Context context, List<BaseModel> data )
	{
		inflater = LayoutInflater.from( context );
		this.data = data;
	}

	public BaseAdapter( Context context, List<BaseModel> data, Boolean showDeleteButton )
	{
		this( context, data );
		this.m_showDeleteButton = showDeleteButton;
	}

	public void delete( int position )
	{
		data.remove( position );
		notifyItemRemoved( position );
	}

	@Override
	public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType )
	{
		View view = inflater.inflate( R.layout.base_item_row, parent, false );
		MyViewHolder holder = new MyViewHolder( view );
		return holder;
	}

	/**
	 * Called by RecyclerView to display the data at the specified position.
	 * This method the given position.
	 * <p/>
	 * Note that unlike {@link ListView}, RecyclerView will not call this method
	 * again if the position of the item changes in the data set unless the item
	 * itself is invalidated or the new position cannot be determined. For this
	 * reason, you should only use the <code>position</code> parameter while
	 * acquiring the related data item inside this method and should not keep a
	 * copy of it. If you need the position of an item later on the updated
	 * adapter position.
	 * 
	 * @param holder
	 *            The ViewHolder which should be updated to represent the
	 *            contents of the item at the given position in the data set.
	 * @param position
	 *            The position of the item within the adapter's data set.
	 */
	@Override
	public void onBindViewHolder( MyViewHolder holder, int position )
	{
		BaseModel current = data.get( position );
		holder.title.setText( current.getTitle() );
		
		holder.rowSubtitle.setVisibility( View.GONE );
		holder.rowSubtitle.setText( "" );
		
		if ( current.getSubtitle() != null )
		{
			if ( !current.getSubtitle().equals( "null" ) )
			{
				holder.rowSubtitle.setVisibility( View.VISIBLE );
				holder.rowSubtitle.setText( current.getSubtitle() );
			}
		}
	}

	@Override
	public int getItemCount()
	{
		return data.size();
	}

	class MyViewHolder extends RecyclerView.ViewHolder
	{
		TextView	title;
		TextView	rowSubtitle;

		public MyViewHolder( View itemView )
		{
			super( itemView );
			title = (TextView) itemView.findViewById( R.id.rowText );
			rowSubtitle = (TextView) itemView.findViewById( R.id.rowSubtitle );
		}
	}
}
