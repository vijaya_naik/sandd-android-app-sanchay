package com.extentor.custom.base.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.sfandroid.R;

/**
 * Created by desmond on 31/5/15.
 */
public class RecyclerAdapterDetails extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	private static final int				TYPE_HEADER			= 1;
	private static final int				TYPE_ITEM			= 0;

	private List<HashMap<String, String>>	mItemList;

	public RecyclerAdapterDetails( )
	{
		super();
		mItemList = new ArrayList<>();
	}

	public void addItems( List<HashMap<String, String>> list )
	{
		mItemList.addAll( list );
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup viewGroup, int viewType )
	{
		Context context = viewGroup.getContext();
		View view;
		if ( viewType == TYPE_HEADER )
		{
			view = LayoutInflater.from( context ).inflate( R.layout.recycler_header, viewGroup, false );
			return new RecyclerHeaderViewHolder( view );
		}
		else if ( viewType == TYPE_ITEM )
		{
			// details list
			view = LayoutInflater.from( context ).inflate( R.layout.recyclerview_detail, viewGroup, false );
			return new RecyclerItemViewHolder( view );
		}

		throw new RuntimeException( "Invalid view type " + viewType );
	}

	@Override
	public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, int position )
	{
		if ( position > 0 )
		{
			RecyclerItemViewHolder holder = (RecyclerItemViewHolder) viewHolder;
			HashMap<String, String> map = mItemList.get( position - 1 );
			String key = map.keySet().iterator().next();
			String val = map.get( key );
			if ( val == null || val.equals( "null" ) )
				val = "";
			holder.mItemTextViewTitle.setText( key );
			holder.mItemTextViewContent.setVisibility( View.VISIBLE );
			holder.mItemBillingAddress.setVisibility( View.GONE );
			holder.mItemImage.setVisibility( View.GONE );
			if( val.contains( "<img src=\"" ) )
			{
				holder.mItemTextViewContent.setVisibility( View.GONE );
				holder.mItemImage.setVisibility( View.VISIBLE );
				if( val.contains( "alt=\"red\"" ) )
				{
					holder.mItemImage.setImageResource( R.drawable.light_red );
				}
				if( val.contains( "alt=\"yellow\"" ) )
				{
					holder.mItemImage.setImageResource( R.drawable.light_yellow );
				}
				if( val.contains( "alt=\"green\"" ) )
				{
					holder.mItemImage.setImageResource( R.drawable.light_green );
				}
			}
			else if( key.equals( "Billing Address" ) )
			{
				holder.mItemTextViewContent.setVisibility( View.GONE );
				holder.mItemBillingAddress.setVisibility( View.VISIBLE );
				try
				{
					JSONObject billingAddress = new JSONObject( val );
					StringBuilder address = new StringBuilder();
					
					if( !billingAddress.isNull( "street" ) )
						address.append( billingAddress.getString( "street" ) + "\n" );
					if( !billingAddress.isNull( "city" ) )
						address.append( billingAddress.getString( "city" ) + "\n" );
					if( !billingAddress.isNull( "state" ) )
						address.append( billingAddress.getString( "state" ) + "\n" );
					if( !billingAddress.isNull( "postalCode" ) )
						address.append( billingAddress.getString( "postalCode" ) + "\n" );
					
					holder.mItemBillingAddress.setText( address.toString() );
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}
			}
			else
			{
				holder.mItemTextViewContent.setText( val );
			}
		}
	}

	@Override
	public int getItemCount()
	{
		return mItemList == null ? 1 : mItemList.size() + 1;
	}

	@Override
	public int getItemViewType( int position )
	{
		return position == 0 ? TYPE_HEADER : TYPE_ITEM;
	}

	private static class RecyclerItemViewHolder extends RecyclerView.ViewHolder
	{
		private final TextView	mItemTextViewTitle, mItemTextViewContent, mItemBillingAddress;
		private final ImageView mItemImage;

		public RecyclerItemViewHolder( View itemView )
		{
			super( itemView );
			mItemTextViewTitle = (TextView) itemView.findViewById( R.id.itemTextViewTitle );
			mItemTextViewContent = (TextView) itemView.findViewById( R.id.itemTextViewContent );
			mItemBillingAddress = (TextView) itemView.findViewById( R.id.billingAddress );
			mItemImage = (ImageView) itemView.findViewById( R.id.itemImage );
		}
	}

	private static class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder
	{

		public RecyclerHeaderViewHolder( View itemView )
		{
			super( itemView );
		}
	}
}
