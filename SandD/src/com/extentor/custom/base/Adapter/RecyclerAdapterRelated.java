package com.extentor.custom.base.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.extentor.helper.Helper;
import com.extentor.sfandroid.R;

/**
 * Created by desmond on 31/5/15.
 */
public class RecyclerAdapterRelated extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	private static final int				TYPE_HEADER	= 1;
	private static final int				TYPE_ITEM	= 0;

	private List<HashMap<String, String>>	mItemList;

	public RecyclerAdapterRelated()
	{
		super();
		mItemList = new ArrayList<>();
	}

	public void addItems( List<HashMap<String, String>> list )
	{
		mItemList.addAll( list );
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup viewGroup, int viewType )
	{
		Context context = viewGroup.getContext();
		View view;
		if ( viewType == TYPE_HEADER )
		{
			view = LayoutInflater.from( context ).inflate( R.layout.recycler_header, viewGroup, false );
			return new RecyclerHeaderViewHolder( view );
		}
		else if ( viewType == TYPE_ITEM )
		{
			// related list
			view = LayoutInflater.from( context ).inflate( R.layout.recyclerview_related, viewGroup, false );
			return new RecyclerItemViewHolder( view );
		}

		throw new RuntimeException( "Invalid view type " + viewType );
	}

	@Override
	public void onBindViewHolder( RecyclerView.ViewHolder viewHolder, int position )
	{
		if ( position > 0 )
		{
			RecyclerItemViewHolder holder = (RecyclerItemViewHolder) viewHolder;
			HashMap<String, String> map = mItemList.get( position - 1 );
			String key = map.keySet().iterator().next();
			holder.mItemTextViewTitle.setText( Helper.ProcessKey( key ) );
		}
	}

	@Override
	public int getItemCount()
	{
		return mItemList == null ? 1 : mItemList.size() + 1;
	}

	@Override
	public int getItemViewType( int position )
	{
		return position == 0 ? TYPE_HEADER : TYPE_ITEM;
	}

	private static class RecyclerItemViewHolder extends RecyclerView.ViewHolder
	{
		private final TextView	mItemTextViewTitle;

		public RecyclerItemViewHolder( View itemView )
		{
			super( itemView );
			mItemTextViewTitle = (TextView) itemView.findViewById( R.id.itemTextViewTitle );
		}
	}

	private static class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder
	{
		public RecyclerHeaderViewHolder( View itemView )
		{
			super( itemView );
		}
	}
}
