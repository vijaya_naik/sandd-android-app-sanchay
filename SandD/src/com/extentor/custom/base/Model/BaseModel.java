package com.extentor.custom.base.Model;

/**
 * Created by Ankur on 7/30/2015.
 */
public class BaseModel
{
	private String	title, subtitle;

	public BaseModel()
	{
		this.subtitle = null;
	}

	public BaseModel( String title )
	{
		this.title = title;
	}

	public BaseModel( String title, String subtitle )
	{
		this.title = title;
		this.subtitle = subtitle;
	}

	public String getTitle()
	{
		return title;
	}
	
	public String getSubtitle()
	{
		return subtitle;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public void setSubtitle( String subtitle )
	{
		this.subtitle = subtitle;
	}
}
