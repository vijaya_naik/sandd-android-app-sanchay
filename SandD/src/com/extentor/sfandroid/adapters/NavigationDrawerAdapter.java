package com.extentor.sfandroid.adapters;

/**
 * Created by Ankur on 7/30/2015.
 */

import java.util.Collections;
import java.util.List;

import com.extentor.custom.base.Model.NavDrawerItem;
import com.extentor.sfandroid.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class NavigationDrawerAdapter extends BaseAdapter
{
	List<NavDrawerItem>		data	= Collections.emptyList();
	private LayoutInflater	inflater;
	private Context			context;

	public NavigationDrawerAdapter( Context context, List<NavDrawerItem> data )
	{
		this.context = context;
		inflater = LayoutInflater.from( context );
		this.data = data;
	}

	@Override
	public int getCount()
	{
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem( int position )
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId( int position )
	{
		// TODO Auto-generated method stub
		return 0;
	}

	private class ViewHolder
	{

		// private ImageView pic;;
		private TextView	name;

	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent )
	{
		// TODO Auto-generated method stub
		inflater = (LayoutInflater) context.getSystemService( Activity.LAYOUT_INFLATER_SERVICE );
		convertView = inflater.inflate( R.layout.item_row, null );
		ViewHolder holder = new ViewHolder();
		holder.name = (TextView) convertView.findViewById( R.id.rowText );
		holder.name.setTextColor( Color.BLACK );
		holder.name.setText( data.get( position ).getTitle() );

		return convertView;
	}
}