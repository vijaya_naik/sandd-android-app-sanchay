package com.extentor.sfandroid.fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.custom.base.Model.BaseModel;
import com.extentor.custom.base.Adapter.BaseAdapter;
import com.extentor.custom.base.Helper.RecyclerItemClickListener;
import com.extentor.sfandroid.activity.BaseFragment;
import com.extentor.sfandroid.activity.Home;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfobjects.ISFObject;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

/**
 * Created by Ankur on 7/30/2015.
 */
public class RecyclerFragment extends BaseFragment
{
	private RecyclerView	mList;
	private BaseAdapter		bAdapter;
	private String			soupName		= null;
	ArrayList<JSONObject>	result			= null;
	List<BaseModel>			data			= new ArrayList<BaseModel>();
	int						recordCount		= 0;

	private boolean			loading			= true;
	int						pastVisiblesItems, visibleItemCount,
			totalItemCount;
	LinearLayoutManager		mLayoutManager;

	final int				kRecordsToFetch	= 20;
	int						limit			= 0, offset = 0;

	ISFObject				m_Object;
	DataTask				dataTask;
	String					lastSearchTerm;
	
	private ProgressBar		progressBar1;

	public RecyclerFragment( ISFObject obj )
	{
		m_Object = obj;
		soupName = obj.getSoupName();
		result = new ArrayList<JSONObject>();
	}

	public void PerformSearch( String search )
	{
		if( lastSearchTerm != null && lastSearchTerm.equals( search ) )
			return;
		
		offset = 0;
		data.clear();
		result.clear();
		bAdapter.notifyDataSetChanged();
		
		if( dataTask != null )
		{
			dataTask.cancel( true );
		}
		
		progressBar1.setVisibility( View.VISIBLE );
		
		if( search == null )
		{	
			lastSearchTerm = null;
			dataTask = new DataTask( soupName );
			dataTask.execute();
			return;
		}
		
		dataTask = new DataTask( soupName, search );
		dataTask.execute();
		lastSearchTerm = search;
	}

	@Nullable
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		View view = inflater.inflate( R.layout.recycler_listview, null );
		
		progressBar1 = (ProgressBar) view.findViewById( R.id.progressBar1 );
		
		setupList( view );
		setUpListData( mList );

		mLayoutManager = new LinearLayoutManager( getActivity() );
		mList.setLayoutManager( mLayoutManager );

		if( dataTask != null )
		{
			dataTask.cancel( true );
		}
		
		progressBar1.setVisibility( View.VISIBLE );
		
		dataTask = new DataTask( soupName );
		dataTask.execute();
		return view;
	}

	/**
	 * Callback method to be invoked when an item in this AdapterView has been
	 * clicked.
	 * <p>
	 * Implementers can call getItemAtPosition(position) if they need to access
	 * the data associated with the selected item.
	 */

	public void setupList( View rootView )
	{
		mList = (RecyclerView) rootView.findViewById( R.id.baseList );
		mList.getItemAnimator().setAddDuration( 1000 );
		mList.getItemAnimator().setChangeDuration( 1000 );
		mList.getItemAnimator().setMoveDuration( 1000 );
		mList.getItemAnimator().setRemoveDuration( 1000 );
		mList.setLayoutManager( new LinearLayoutManager( getActivity() ) );
		mList.addOnScrollListener( new RecyclerView.OnScrollListener()
		{
			@Override
			public void onScrolled( RecyclerView recyclerView, int dx, int dy )
			{
				visibleItemCount = mLayoutManager.getChildCount();
				totalItemCount = mLayoutManager.getItemCount();
				pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

				if ( loading )
				{
					if ( ( visibleItemCount + pastVisiblesItems ) >= totalItemCount )
					{
						loading = false;
						if( lastSearchTerm == null )
						{
							if( dataTask.getStatus() != AsyncTask.Status.RUNNING )
							{
								dataTask = new DataTask( soupName );
								dataTask.execute();
							}
						}
					}
				}
			}
		} );
	}

	private void setUpListData( RecyclerView mList )
	{
		bAdapter = new BaseAdapter( getActivity(), data );
		mList.setAdapter( bAdapter );
		mList.addOnItemTouchListener( new RecyclerItemClickListener( getActivity(), new RecyclerItemClickListener.OnItemClickListener()
		{
			@Override
			public void onItemClick( View view, int position )
			{
				callNavigation( view, position );
			}
		} ) );
	}

	public void addData( JSONObject obj )
	{
		BaseModel navItem = new BaseModel();
		String name = "";
		try
		{
			name = obj.getString( "Name" );
			navItem.setTitle( name );
			
			if ( obj.has( "Division__c" ) )
			{
				name = obj.getString( "Division__c" );
				navItem.setSubtitle( name );
			}
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		data.add( navItem );
	}

	private void callNavigation( View view, int position )
	{
		m_Object.setObject( result.get( position ) );
		Home.doRecordViewNavigation( getActivity(), m_Object );
	}

	private class DataTask extends AsyncTask<Void, Void, Void>
	{
		private String	soupName, search;
		JSONArray		_result	= null, _result2 = null;

		public DataTask( String soupName )
		{
			this.soupName = soupName;
		}

		public DataTask( String soupName, String search )
		{
			this.soupName = soupName;
			this.search = search;
		}

		@Override
		protected Void doInBackground( Void... params )
		{
			SmartStore store = SFApp.GetSmartStore();
			try
			{
				if ( recordCount == 0 )
				{
					_result = store.query( QuerySpec.buildSmartQuerySpec( "select Count(*) from {" + this.soupName + "}", 10 ), 0 );
					recordCount = _result.getJSONArray( 0 ).getInt( 0 );
				}

				if ( ( recordCount - result.size() ) < kRecordsToFetch )
					limit = recordCount - result.size();
				else
					limit = kRecordsToFetch;

				String WHERE = "";
//				if ( this.soupName.equals( "Account" ) )
//				{
//					WHERE = " where {" + this.soupName + ":RecordTypeId} in (select {RecordType:Id} from {RecordType} where {RecordType:Name}='Distributor') ";
//					if( this.search != null )
//					{
//						WHERE = " and {" + this.soupName + ":RecordTypeId} in (select {RecordType:Id} from {RecordType} where {RecordType:Name}='Distributor') ";
//					}
//				}
				String query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "}" + WHERE +  " order by {" + this.soupName + ":Name}";
				if( this.search != null )
				{
					query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "} where {" + this.soupName + ":Name} like '%" + this.search + "%'" + WHERE + " order by {" + this.soupName + ":Name}";
				}
				
//				if ( this.soupName.equals( "Order__c" ) )
//				{
//					query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "}" + WHERE +  " order by {" + this.soupName + ":Order_Date__c} DESC";
//					if( this.search != null )
//					{
//						query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "} where {" + this.soupName + ":Name} like '%" + this.search + "%'" + WHERE + " order by {" + this.soupName + ":Order_Date__c} DESC";
//					}
//				}
				
				_result2 = store.query( QuerySpec.buildSmartQuerySpec( query, limit ), offset++ );
				int size = _result2.length();

				for ( int i = 0; i < size; i++ )
				{
					JSONArray arr = _result2.getJSONArray( i );
					JSONObject obj = arr.getJSONObject( 0 );
					result.add( obj );
					addData( obj );
				}
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute( Void result )
		{
			loading = true;
			bAdapter.notifyDataSetChanged();
			progressBar1.setVisibility( View.GONE );
		}
	}
}
