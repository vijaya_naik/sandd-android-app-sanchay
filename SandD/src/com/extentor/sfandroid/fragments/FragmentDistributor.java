package com.extentor.sfandroid.fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.extentor.custom.base.Adapter.BaseAdapter;
import com.extentor.custom.base.Helper.RecyclerItemClickListener;
import com.extentor.custom.base.Model.BaseModel;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfandroid.activity.AddOrderActivity;
import com.extentor.sfandroid.activity.BaseFragment;
import com.extentor.sfobjects.ISFObject;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

public class FragmentDistributor extends BaseFragment
{
	RecyclerView			mList;

	private BaseAdapter		bAdapter;
	private String			soupName		= null;
	ArrayList<JSONObject>	result			= null;
	List<BaseModel>			data			= new ArrayList<BaseModel>();
	int						recordCount		= 0;

	private boolean			loading			= true;
	int						pastVisiblesItems, visibleItemCount,
			totalItemCount;
	LinearLayoutManager		mLayoutManager;

	final int				kRecordsToFetch	= 20;
	int						limit			= 0, offset = 0;

	ISFObject				m_Object;
	DataTask				dataTask;
	String					lastSearchTerm;
	private Button			editTextClear;
	
	private ProgressBar		progressBar1;

	public FragmentDistributor()
	{

	}

	@Override
	public void setArguments( Bundle args )
	{
		super.setArguments( args );
	}

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
	}
	
	public void Close()
	{
		InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService( Activity.INPUT_METHOD_SERVICE );
		inputMethodManager.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0 );
		
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.popBackStack();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.remove( FragmentDistributor.this );
		fragmentTransaction.commit();
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		View view = inflater.inflate( R.layout.fragment_distributor, null );
		
		progressBar1 = (ProgressBar) view.findViewById( R.id.progressBar1 );
		
		ImageView navigationmenu_handle_left = (ImageView) view.findViewById( R.id.navigationmenu_handle_left );
		navigationmenu_handle_left.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				Close();
			}
		} );

		final EditText editTextSearch = (EditText) view.findViewById( R.id.editTextSearch );
		editTextSearch.setImeOptions( EditorInfo.IME_ACTION_DONE );
		editTextSearch.addTextChangedListener( new TextWatcher()
		{
			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count )
			{
				// get text, fire text to search
				PerformSearch( s.toString() );
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after )
			{
			}

			@Override
			public void afterTextChanged( Editable s )
			{
			}
		} );
		editTextClear = (Button) view.findViewById( R.id.editTextClear );
		editTextClear.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				editTextSearch.setText( "" );

				InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService( Activity.INPUT_METHOD_SERVICE );
				inputMethodManager.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0 );

				PerformSearch( null );
			}
		} );

		setupList( view );
		setUpListData( mList );

		mLayoutManager = new LinearLayoutManager( getActivity() );
		mList.setLayoutManager( mLayoutManager );
		
		progressBar1.setVisibility( View.VISIBLE );

		soupName = "Account";
		result = new ArrayList<JSONObject>();
		if ( dataTask != null )
		{
			dataTask.cancel( true );
		}
		dataTask = new DataTask( soupName );
		dataTask.execute();

		return view;
	}

	public void PerformSearch( String search )
	{
		if ( lastSearchTerm != null && lastSearchTerm.equals( search ) )
			return;

		offset = 0;
		data.clear();
		result.clear();
		bAdapter.notifyDataSetChanged();

		if ( dataTask != null )
		{
			dataTask.cancel( true );
		}

		progressBar1.setVisibility( View.VISIBLE );
		
		if ( search == null )
		{
			lastSearchTerm = null;
			dataTask = new DataTask( soupName );
			dataTask.execute();
			return;
		}

		dataTask = new DataTask( soupName, search );
		dataTask.execute();
		lastSearchTerm = search;
	}

	public void setupList( View rootView )
	{
		mList = (RecyclerView) rootView.findViewById( R.id.baseList );
		mList.getItemAnimator().setAddDuration( 1000 );
		mList.getItemAnimator().setChangeDuration( 1000 );
		mList.getItemAnimator().setMoveDuration( 1000 );
		mList.getItemAnimator().setRemoveDuration( 1000 );
		mList.setLayoutManager( new LinearLayoutManager( getActivity() ) );
		mList.addOnScrollListener( new RecyclerView.OnScrollListener()
		{
			@Override
			public void onScrolled( RecyclerView recyclerView, int dx, int dy )
			{
				visibleItemCount = mLayoutManager.getChildCount();
				totalItemCount = mLayoutManager.getItemCount();
				pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

				if ( loading )
				{
					if ( ( visibleItemCount + pastVisiblesItems ) >= totalItemCount )
					{
						loading = false;
						if( lastSearchTerm == null )
						{
							DataTask dataTask = new DataTask( soupName );
							dataTask.execute();
						}
					}
				}
			}
		} );
	}

	private void setUpListData( RecyclerView mList )
	{
		bAdapter = new BaseAdapter( getActivity(), data );
		mList.setAdapter( bAdapter );
		mList.addOnItemTouchListener( new RecyclerItemClickListener( getActivity(), new RecyclerItemClickListener.OnItemClickListener()
		{
			@Override
			public void onItemClick( View view, int position )
			{
				((AddOrderActivity)getActivity()).UpdateDistributor( "dist", result.get( position ) );
				Close();
			}
		} ) );
	}

	public void addData( JSONObject obj )
	{
		BaseModel navItem = new BaseModel();
		String name = "";
		try
		{
			name = obj.getString( "Name" );
			navItem.setTitle( name );
			
			name = obj.getString( "Division__c" );
			navItem.setSubtitle( name );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		data.add( navItem );
	}

	private class DataTask extends AsyncTask<Void, Void, Void>
	{
		private String	soupName, search;
		JSONArray		_result	= null, _result2 = null;

		public DataTask( String soupName )
		{
			this.soupName = soupName;
		}

		public DataTask( String soupName, String search )
		{
			this.soupName = soupName;
			this.search = search;
		}

		@Override
		protected Void doInBackground( Void... params )
		{
			// TODO Auto-generated method stub
			SmartStore store = SFApp.GetSmartStore();
			try
			{
				if ( recordCount == 0 )
				{
					_result = store.query( QuerySpec.buildSmartQuerySpec( "select Count(*) from {" + this.soupName + "}", 10 ), 0 );
					recordCount = _result.getJSONArray( 0 ).getInt( 0 );
				}

				if ( ( recordCount - result.size() ) < kRecordsToFetch )
					limit = recordCount - result.size();
				else
					limit = kRecordsToFetch;
				
				String WHERE = "";
				if ( this.soupName.equals( "Account" ) )
				{
					WHERE = " where {" + this.soupName + ":RecordTypeId} in (select {RecordType:Id} from {RecordType} where {RecordType:Name}='Distributor') ";
					if( this.search != null )
					{
						WHERE = " and {" + this.soupName + ":RecordTypeId} in (select {RecordType:Id} from {RecordType} where {RecordType:Name}='Distributor') ";
					}
				}
				String query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "}" + WHERE +  " order by {" + this.soupName + ":Name}";
				if( this.search != null )
				{
					query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "} where {" + this.soupName + ":Name} like '%" + this.search + "%'" + WHERE + " order by {" + this.soupName + ":Name}";
				}

				_result2 = store.query( QuerySpec.buildSmartQuerySpec( query, limit ), offset++ );
				int size = _result2.length();

				for ( int i = 0; i < size; i++ )
				{
					JSONArray arr = _result2.getJSONArray( i );
					JSONObject obj = arr.getJSONObject( 0 );
					result.add( obj );
					addData( obj );
				}
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute( Void result )
		{
			loading = true;
			bAdapter.notifyDataSetChanged();
			progressBar1.setVisibility( View.GONE );
		}
	}
}
