package com.extentor.sfandroid.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.extentor.custom.base.Adapter.RecyclerAdapterDetails;
import com.extentor.custom.base.Adapter.RecyclerAdapterRelated;
import com.extentor.custom.base.CustomView.ViewPager.RecyclerViewFragment;
import com.extentor.custom.base.Helper.RecyclerItemClickListener;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.extentor.sfandroid.R;
import com.extentor.sfandroid.activity.Home;
import com.extentor.sfobjects.ISFObject;

/**
 * Created by Ankur on 7/31/2015.
 */
public class BaseRecyclerViewFragment extends RecyclerViewFragment
{
	public static final String	TAG	= BaseRecyclerViewFragment.class.getSimpleName();
	private LinearLayoutManager	mLayoutMgr;

	JSONObject					details;
	JSONObject					fields;
	JSONArray					related;

	int							tabPosition;
	TextView					textViewNoRecords;
	ISFObject					m_Object;

	public static Fragment newInstance( int position )
	{
		BaseRecyclerViewFragment fragment = new BaseRecyclerViewFragment();
		Bundle args = new Bundle();
		args.putInt( ARG_POSITION, position );
		fragment.setArguments( args );
		return fragment;
	}

	public void SetDataSources( JSONObject details, JSONObject fields, JSONArray related )
	{
		this.details = details;
		this.fields = fields;
		this.related = related;
	}

	public BaseRecyclerViewFragment()
	{
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		mPosition = getArguments().getInt( ARG_POSITION );

		View view = inflater.inflate( R.layout.fragment_recycler_view, container, false );
		mRecyclerView = (RecyclerView) view.findViewById( R.id.recyclerView );
		textViewNoRecords = (TextView) view.findViewById( R.id.textViewNoRecords );

		setupRecyclerView();

		return view;
	}

	@Override
	protected void setScrollOnLayoutManager( int scrollY )
	{
		mLayoutMgr.scrollToPositionWithOffset( 0, -scrollY );
	}

	private void setupRecyclerView()
	{
		mLayoutMgr = new LinearLayoutManager( getActivity() );
		mRecyclerView.setLayoutManager( mLayoutMgr );

		RecyclerView.Adapter<RecyclerView.ViewHolder> recyclerAdapter = null;
		if ( this.getArguments().getInt( ARG_POSITION ) == 0 )
		{
			recyclerAdapter = new RecyclerAdapterDetails();
			( (RecyclerAdapterDetails) recyclerAdapter ).addItems( createDetailList() );
		}
		else
		{
			recyclerAdapter = new RecyclerAdapterRelated();
			( (RecyclerAdapterRelated) recyclerAdapter ).addItems( createRelatedList() );

			mRecyclerView.addOnItemTouchListener( new RecyclerItemClickListener( getActivity(), new RecyclerItemClickListener.OnItemClickListener()
			{
				@Override
				public void onItemClick( View view, int position )
				{
					if ( position != 0 )
						callNavigation( view, position );
				}
			} ) );
		}

		mRecyclerView.setAdapter( recyclerAdapter );
		setRecyclerViewOnScrollListener();
	}

	private List<HashMap<String, String>> createDetailList()
	{
		int count = details.length();

		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		for ( int i = 0; i < count; i++ )
		{
			try
			{
				String key = details.names().getString( i );
				String val = details.getString( key );
				key = fields.getString( key );
				HashMap<String, String> map = new HashMap<>();
				map.put( key, val );
				list.add( map );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
		}
		return list;
	}

	private List<HashMap<String, String>> createRelatedList()
	{
		int count = related.length();

		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

		if ( count == 0 )
		{
			textViewNoRecords.setVisibility( View.VISIBLE );
			return list;
		}

		for ( int i = 0; i < count; i++ )
		{
			try
			{
				JSONObject obj = related.getJSONObject( i );
				HashMap<String, String> map = new HashMap<>();
				map.put( obj.getString( "name" ), null );
				list.add( map );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
		}
		return list;
	}

	private void callNavigation( View view, int position )
	{
		ISFObject sfObject = null;
		JSONObject objData = null;
		try
		{
			JSONObject obj = related.getJSONObject( position - 1 );
			sfObject = (ISFObject) obj.get( "object" );
			objData = new JSONObject();
			objData.put( "id", obj.getString( "id" ) );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		sfObject.setObject( objData );
		Home.doListViewNavigation( getActivity(), view, sfObject );
	}
}
