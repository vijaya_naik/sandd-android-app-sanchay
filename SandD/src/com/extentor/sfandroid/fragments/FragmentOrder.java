package com.extentor.sfandroid.fragments;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemSelectedListener;
import com.extentor.custom.base.Adapter.BaseAdapter;
import com.extentor.custom.base.Helper.RecyclerItemClickListener;
import com.extentor.custom.base.Model.BaseModel;
import com.extentor.dialogs.SyncProgressDialog;
import com.extentor.helper.Helper;
import com.extentor.interfaces.IDataFetch;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfandroid.activity.BaseFragment;
import com.extentor.sfandroid.ui.DateInputField;
import com.extentor.sfandroid.ui.IRenderer;
import com.extentor.sfandroid.ui.InputField;
import com.extentor.sfandroid.ui.SpinnerInputField;
import com.extentor.sfandroid.ui.TextInputField;
import com.extentor.sfandroid.ui.UIRenderer;
import com.extentor.task.Executor;
import com.extentor.task.PushNewObject;
import com.extentor.task.TaskQueue;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

public class FragmentOrder extends BaseFragment
{
	RecyclerView			mList;
	private BaseAdapter		bAdapter;

	Boolean					isDateSelected		= false;
	Boolean					isAccountSelected	= false;
	Boolean					isPricebookSelected	= false;
	Boolean					isShiptoSelected	= false;
	Boolean					isPONumberSelected	= false;

	// data for display
	List<BaseModel>			data				= new ArrayList<BaseModel>();

	// data for order - distributor and order items
	JSONObject				m_Distributor;
	ArrayList<JSONObject>	m_OrderItems		= new ArrayList<JSONObject>();
	ArrayList<JSONObject>	m_OrderItemsValues	= new ArrayList<JSONObject>();

	JSONArray				m_orders;

	SyncProgressDialog		progress			= null;
	public Location			location			= null;

	private UIRenderer		renderer;

	final JSONObject		orderData			= new JSONObject();
	final JSONObject		displayData			= new JSONObject();

	ArrayList<String>		listAccounts		= new ArrayList<String>();
	ArrayList<String>		listPriceBooks		= new ArrayList<String>();
	ArrayList<String>		listShipTo			= new ArrayList<String>();

	String					selectedAccount, selectedPriceBook, selectedShipTo;
	
	SpinnerInputField		account;
	ArrayList<String>		accounts;
	int						autoSelectedAccountIndex	= 0;

	public FragmentOrder()
	{

	}

	@Override
	public void setArguments( Bundle args )
	{
		super.setArguments( args );
	}

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
	}

	private Boolean Validate()
	{
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder( getActivity() );
		String message = "";
		if ( !isDateSelected )
			message = "Please select date";
		else if ( !isAccountSelected )
			message = "Please select account";
		else if ( !isPricebookSelected )
			message = "Please select price book";
		else if ( !isShiptoSelected )
			message = "Please select a shipping address";
		dialogBuilder.setMessage( message );
		Boolean result = isDateSelected && isAccountSelected && isPricebookSelected && isShiptoSelected;
		if ( !result )
			dialogBuilder.create().show();
		return result;
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		View view = inflater.inflate( R.layout.list_common, null );
		ImageView navigationmenu_handle_left = (ImageView) view.findViewById( R.id.navigationmenu_handle_left );
		navigationmenu_handle_left.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				getActivity().onBackPressed();
			}
		} );

		Button buttonAddOrderItem = (Button) view.findViewById( R.id.buttonAddOrderItem );
		buttonAddOrderItem.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				if ( selectedAccount == null )
				{
					AlertDialog.Builder dialogBuilder = new AlertDialog.Builder( getActivity() );
					dialogBuilder.setMessage( "Please select an account first" );
					dialogBuilder.create().show();
					return;
				}
				if ( selectedPriceBook == null )
				{
					AlertDialog.Builder dialogBuilder = new AlertDialog.Builder( getActivity() );
					dialogBuilder.setMessage( "Please select a price book" );
					dialogBuilder.create().show();
					return;
				}
				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.add( R.id.base_container, new FragmentOrderItem( selectedPriceBook ) );
				fragmentTransaction.commit();
			}
		} );

		Button buttonCreateOrder = (Button) view.findViewById( R.id.buttonCreateOrder );
		buttonCreateOrder.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				if ( Validate() )
				{
					SaveOrder();
				}
			}
		} );

		setupList( view );
		setUpListData( mList );
		
		CreateForm( view );

		if ( getArguments() != null )
		{
			if ( getArguments().containsKey( "dist" ) )
			{
				String dist = getArguments().getString( "dist" );
				try
				{
					JSONObject obj = new JSONObject( dist );
					UpdateDistributor( obj );
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}
			}
		}

		return view;
	}

	private void CreateForm( View view )
	{
		LinearLayout myLayout = (LinearLayout) view.findViewById( R.id.ll_parent_form );
		if ( renderer == null )
		{
			renderer = new UIRenderer( myLayout );
		}

		InputField ifDate = new DateInputField( getActivity(), "Date:", R.layout.edit_text );
		InputField.Bind( ifDate, displayData, getString( R.string.ORDER_ITEM_DATE ) );
		renderer.AddUIControl( ifDate );
		( (DateInputField) ifDate ).setOnDateSetListener( new OnDateSetListener()
		{
			@Override
			public void onDateSet( DatePicker view, int year, int monthOfYear, int dayOfMonth )
			{
				try
				{
					TimeZone tz = TimeZone.getDefault();
					Date now = new Date();
					int offsetFromUtc = tz.getOffset( now.getTime() );
					GregorianCalendar gCal = new GregorianCalendar( year, monthOfYear, dayOfMonth );
					long time = gCal.getTimeInMillis() + offsetFromUtc;
					orderData.put( "Date__c", time );
					isDateSelected = true;
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}
			}
		} );

		final SpinnerInputField shipTo = new SpinnerInputField( getActivity().getBaseContext(), "Ship To:", R.layout.dropdown );

//		AutocompleteTextInputField account = new AutocompleteTextInputField( getActivity(), "Account:", R.layout.auto_complete );
		account = new SpinnerInputField( getActivity(), "Account:", R.layout.dropdown );
		InputField.Bind( account, displayData, "account" );
		renderer.AddUIControl( account );
		account.setOnItemSelectedListener( new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
			{
				if ( position != 0 )
				{
					selectedAccount = listAccounts.get( position - 1 );
					Helper.SetKeyValue( orderData, "Buyer__c", selectedAccount );
					ArrayList<String> shipTos = Query( "Select...", "select {Shipping_Address__c:Id}, {Shipping_Address__c:Name} from {Shipping_Address__c} where {Shipping_Address__c:Account__c}='" + selectedAccount + "'", listShipTo );
					shipTo.UpdateData( shipTos );
					isAccountSelected = true;
				}
			}

			@Override
			public void onNothingSelected( AdapterView<?> arg0 )
			{
			}
		} );
		accounts = Query( "Select Account", "select {Account:Id}, {Account:Name} from {Account}", listAccounts );
		account.SetData( accounts );

		SpinnerInputField priceBook = new SpinnerInputField( getActivity().getBaseContext(), "Price Book:", R.layout.dropdown );
		SpinnerInputField.Bind( priceBook, displayData, getString( R.string.ORDER_ITEM_PRICE_BOOK ) );
		renderer.AddUIControl( priceBook );
		priceBook.setOnItemSelectedListener( new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
			{
				if ( position != 0 )
				{
					selectedPriceBook = listPriceBooks.get( position - 1 );
					Helper.SetKeyValue( orderData, "Price_Book__c", selectedPriceBook );
					isPricebookSelected = true;
				}
			}

			@Override
			public void onNothingSelected( AdapterView<?> arg0 )
			{
			}
		} );
		ArrayList<String> priceBooks = Query( "Select Price Book", "select {Pricebook2:Id}, {Pricebook2:Name} from {Pricebook2}", listPriceBooks );
		priceBook.SetData( priceBooks );

		SpinnerInputField.Bind( shipTo, displayData, getString( R.string.ORDER_ITEM_SHIP_TO ) );
		renderer.AddUIControl( shipTo );
		shipTo.setOnItemSelectedListener( new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
			{
				if ( position != 0 )
				{
					selectedShipTo = listShipTo.get( position - 1 );
					Helper.SetKeyValue( orderData, "Shipping_Address_Name__c", selectedShipTo );
					isShiptoSelected = true;
				}
			}

			@Override
			public void onNothingSelected( AdapterView<?> arg0 )
			{
			}
		} );
		ArrayList<String> shipTos = Query( "Select...", "select {Shipping_Address__c:Id}, {Shipping_Address__c:Name} from {Shipping_Address__c} where {Shipping_Address__c:Account__c}='" + selectedAccount + "'", listShipTo );
		shipTo.SetData( shipTos );

		TextInputField poNumber = new TextInputField( getActivity().getBaseContext(), "PO Number:", R.layout.edit_text );
		InputField.Bind( poNumber, orderData, "PO_Reference__c" );
		InputField.Bind( poNumber, displayData, getString( R.string.ORDER_ITEM_QUANTITY ) );
		renderer.AddUIControl( poNumber );

		renderer.Render( new IRenderer()
		{
			@Override
			public void RenderingDone()
			{
				account.selectItem( autoSelectedAccountIndex );
			}
		} );
	}

	public ArrayList<String> Query( String first, String query, ArrayList<String> items )
	{
		ArrayList<String> list = new ArrayList<String>();
		SmartStore store = SFApp.GetSmartStore();
		JSONArray result = null;
		try
		{
			result = store.query( QuerySpec.buildSmartQuerySpec( query, 500 ), 0 );
			list.add( first );
			int count = result.length();
			for ( int i = 0; i < count; i++ )
			{
				items.add( result.getJSONArray( i ).getString( 0 ) );
				list.add( result.getJSONArray( i ).getString( 1 ) );
			}
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		return list;
	}

	private void ClearData()
	{
		isDateSelected = false;

		data.clear();
		bAdapter.notifyDataSetChanged();
		isAccountSelected = false;

		m_Distributor = null;
		m_OrderItems.clear();
	}

	private void SaveOrder()
	{
		String m_orders_str = Helper.GetSharedPrefItem( getString( R.string.NEW_ORDER ) );
		if ( m_orders_str != null )
		{
			try
			{
				m_orders = new JSONArray( m_orders_str );
			}
			catch ( JSONException e1 )
			{
				e1.printStackTrace();
			}
		}
		if ( m_orders == null )
		{
			m_orders = new JSONArray();
		}

		JSONObject objectContainer = new JSONObject();
		try
		{
			JSONObject orderRecord = new JSONObject();
			orderRecord.put( "Date__c", orderData.get( "Date__c" ) );
			orderRecord.put( "Price_Book__c", selectedPriceBook );
			orderRecord.put( "Buyer__c", selectedAccount );
			if ( orderData.has( "PO_Reference__c" ) )
				orderRecord.put( "PO_Reference__c", orderData.get( "PO_Reference__c" ) );
			orderRecord.put( "Shipping_Address_Name__c", orderData.get( "Shipping_Address_Name__c" ) );

			objectContainer.put( "orderRecord", orderRecord );

			int count = m_OrderItems.size();
			JSONArray order_line_items = new JSONArray();
			for ( int i = 0; i < count; i++ )
			{
				JSONObject attrContainer2 = new JSONObject();

				JSONObject temp = m_OrderItems.get( i );
				Iterator<String> keys = temp.keys();

				while ( keys.hasNext() )
				{
					String key = keys.next();

					if ( temp.get( key ) instanceof Long || temp.get( key ) instanceof Integer )
					{
						Long val = temp.getLong( key );
						attrContainer2.put( key, val );
					}
					else
					{
						String val = temp.getString( key );
						attrContainer2.put( key, val );
					}
				}
				order_line_items.put( attrContainer2 );
			}
			// #2
			objectContainer.put( "orderLineItemRecord", order_line_items );

			// #3 display data
			objectContainer.put( getString( R.string.ORDER_DISPLAY_DATA ), m_OrderItemsValues.toString() );

			m_orders.put( objectContainer );

			ClearData();

			Helper.EditSharedPrefs( getString( R.string.NEW_ORDER ), m_orders.toString() );

			AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
			builder.setTitle( "Order Saved" );
			builder.setMessage( "Kindly sync to send the new order to the server. Do you want to sync now?" );
			builder.setPositiveButton( "Yes", new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick( DialogInterface dialog, int which )
				{
					// perform sync
					SyncProgressDialog.showProgressBar( getActivity(), false );
					progress = SyncProgressDialog.getProgressDialog();

					final TaskQueue queue = TaskQueue.getInstance();
					queue.Clear();
					queue.AddTask( new Executor( new PushNewObject( getActivity(), new IDataFetch()
					{
						@Override
						public void onDataFetchSuccess()
						{
							SyncProgressDialog.hideProgressBar();
							AlertDialog.Builder alertDialog = new AlertDialog.Builder( getActivity() );
							alertDialog.setTitle( "Success" );
							alertDialog.setMessage( "Orders successfully synced!" );
							alertDialog.create().show();
						}

						@Override
						public void onDataFetchStart()
						{
						}

						@Override
						public void onDataFetchError( Exception exception )
						{
							SyncProgressDialog.hideProgressBar();
							AlertDialog.Builder alertDialog = new AlertDialog.Builder( getActivity() );
							alertDialog.setTitle( "Error" );
							alertDialog.setMessage( "Orders sync failure!" );
							alertDialog.create().show();
						}
					} ) ) );
					queue.RunNext();
				}
			} );
			builder.setNegativeButton( "No", new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick( DialogInterface dialog, int which )
				{
				}
			} );
			builder.create().show();
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void setupList( View rootView )
	{
		mList = (RecyclerView) rootView.findViewById( R.id.baseList );
		mList.getItemAnimator().setAddDuration( 1000 );
		mList.getItemAnimator().setChangeDuration( 1000 );
		mList.getItemAnimator().setMoveDuration( 1000 );
		mList.getItemAnimator().setRemoveDuration( 1000 );
		mList.setLayoutManager( new LinearLayoutManager( getActivity() ) );
	}

	private void setUpListData( RecyclerView mList )
	{
		bAdapter = new BaseAdapter( getActivity(), data, true );
		mList.setAdapter( bAdapter );
		mList.addOnItemTouchListener( new RecyclerItemClickListener( getActivity(), new RecyclerItemClickListener.OnItemClickListener()
		{
			@Override
			public void onItemClick( View view, final int position )
			{
				// display pop up menu for actions
				AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
				builder.setTitle( "Select Action" ).setItems( new String[]{"View", "Delete"}, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick( DialogInterface dialog, int which )
					{
						switch ( which )
						{
							case 0 :
								// view
								viewOrderItem( position );
								break;
							case 1 :
								// delete
								deleteOrderItem( position );
								break;
							default :
								break;
						}
					}
				} );
				builder.create().show();
			}
		} ) );
	}

	private void viewOrderItem( int position )
	{
		JSONObject item = m_OrderItemsValues.get( position );
		ArrayList<String> keys = new ArrayList<>();
		keys.add( getString( R.string.ORDER_ITEM_PRODUCT_CODE ) );
		keys.add( getString( R.string.ORDER_ITEM_PRODUCT_NAME ) );
		keys.add( getString( R.string.ORDER_ITEM_PRICE_BOOK ) );
		keys.add( getString( R.string.ORDER_ITEM_PRICE ) );
		keys.add( getString( R.string.ORDER_ITEM_QUANTITY ) );
		keys.add( getString( R.string.ORDER_ITEM_UOM ) );
		keys.add( getString( R.string.ORDER_ITEM_DISCOUNT ) );
		keys.add( getString( R.string.ORDER_ITEM_SALES_PRICE ) );
		keys.add( getString( R.string.ORDER_ITEM_NET_PRICE ) );

		final Dialog dialog = new Dialog( getActivity(), R.style.Theme_AppCompat_Light_Dialog_Alert );
		dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
		View view = getActivity().getLayoutInflater().inflate( R.layout.recycler_listview, null );
		RecyclerView mList = (RecyclerView) view.findViewById( R.id.baseList );
		mList.getItemAnimator().setAddDuration( 1000 );
		mList.getItemAnimator().setChangeDuration( 1000 );
		mList.getItemAnimator().setMoveDuration( 1000 );
		mList.getItemAnimator().setRemoveDuration( 1000 );
		mList.setLayoutManager( new LinearLayoutManager( getActivity() ) );
		List<BaseModel> data = new ArrayList<BaseModel>();

		for ( String key : keys )
		{
			if ( !item.isNull( key ) )
			{
				BaseModel navItem = new BaseModel();
				String name = "";
				try
				{
					navItem.setTitle( key );
					name = item.getString( key );
					navItem.setSubtitle( name );
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}
				data.add( navItem );
			}
		}

		mList.setAdapter( new BaseAdapter( getActivity(), data ) );
		dialog.setContentView( view );
		dialog.show();
	}

	private void deleteOrderItem( int position )
	{
		data.remove( position );
		bAdapter.notifyDataSetChanged();
		if ( data.size() == 0 )
		{
			isAccountSelected = false;
		}
	}

	public void addData( JSONObject obj )
	{
		BaseModel navItem = new BaseModel();
		String name = "";
		try
		{
			name = obj.getString( getString( R.string.ORDER_ITEM_PRODUCT_NAME ) );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		navItem.setTitle( name );
		data.add( navItem );
	}

	public void UpdateDistributor( JSONObject object )
	{
		m_Distributor = object;
		isDateSelected = true;
		try
		{
			String name = m_Distributor.getString( "Name" );
			int count = accounts.size();
			for( ; autoSelectedAccountIndex < count; autoSelectedAccountIndex++ )
			{
				if( name.equals( accounts.get( autoSelectedAccountIndex ) ) )
				{
					break;
				}
			}
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void UpdateOrderItem( JSONObject displayData, JSONObject orderData )
	{
		isAccountSelected = true;
		m_OrderItems.add( orderData );
		m_OrderItemsValues.add( displayData );
		addData( displayData );
		bAdapter.notifyDataSetChanged();
	}
}
