package com.extentor.sfandroid.fragments;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.extentor.helper.Helper;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfandroid.activity.AddOrderActivity;
import com.extentor.sfandroid.activity.BaseFragment;
import com.extentor.sfandroid.ui.InputField;
import com.extentor.sfandroid.ui.LabelTextField;
import com.extentor.sfandroid.ui.TextInputField;
import com.extentor.sfandroid.ui.SpinnerInputField;
import com.extentor.sfandroid.ui.UIRenderer;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

public class FragmentOrderItem extends BaseFragment
{
	private UIRenderer	renderer;
	SpinnerInputField	productCode, productName, priceBook, scheme;
	TextInputField		quantity, discount;
	LabelTextField		salesPrice, netPrice, category, price, UOM;
	InputField			priceUnit;
	InputField			proposedDOD, remarks;
	String				subtypeQuery		= "''";
	ArrayList<String>	listCrops;
	ArrayList<String>	listProductIds		= new ArrayList<String>();
	String				listPriceListId;
	ArrayList<String>	listUOM, listSchemes;
	String				selectedProductId, selectedPriceListId;
	double				d_unitPrice;
	int					i_priceUnit;
	final JSONObject	orderData			= new JSONObject();
	final JSONObject	displayData			= new JSONObject();

	Boolean				selectedProduct		= false;
	Boolean				selectedQuantity	= false;
	Boolean				m_isSungro			= false;

	String				m_selectedPriceBookId, m_computedPrice;

	JSONArray			priceBookEntries, products, priceBooks;

	public FragmentOrderItem( String selectedPriceBook )
	{
		m_selectedPriceBookId = selectedPriceBook;
		Helper.SetKeyValue( orderData, "Price_Book__c", m_selectedPriceBookId );
	}

	@Override
	public void setArguments( Bundle args )
	{
		super.setArguments( args );
	}

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
	}

	public void Close()
	{
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.popBackStack();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.remove( FragmentOrderItem.this );
		fragmentTransaction.commit();
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		View view = inflater.inflate( R.layout.fragment_order_item, null );
		ImageView navigationmenu_handle_left = (ImageView) view.findViewById( R.id.navigationmenu_handle_left );
		navigationmenu_handle_left.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				Close();
			}
		} );

		Button buttonCreateOrder = (Button) view.findViewById( R.id.buttonCreateOrder );
		buttonCreateOrder.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				if ( Validate() )
				{
					( (AddOrderActivity) getActivity() ).UpdateOrderItem( displayData, orderData );
					Close();
				}
			}
		} );

		CreateForm( view );
		return view;
	}

	private Boolean Validate()
	{
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder( getActivity() );
		String message = "";

		if ( !selectedProduct )
			message = "Please select product";
		else if ( !selectedQuantity )
			message = "Please select quantity";
		dialogBuilder.setMessage( message );
		Boolean result = selectedProduct && selectedQuantity;
		if ( !result )
			dialogBuilder.create().show();
		return result;
	}

	private void CreateForm( View view )
	{
		LinearLayout myLayout = (LinearLayout) view.findViewById( R.id.ll_parent_form );
		if ( renderer == null )
		{
			renderer = new UIRenderer( myLayout );
		}

		Query();

		productCode = new SpinnerInputField( getActivity().getBaseContext(), "Product Code:", R.layout.dropdown );
		productCode.SetData( new ArrayList<String>() );
		SpinnerInputField.Bind( productCode, displayData, getString( R.string.ORDER_ITEM_PRODUCT_CODE ) );
		renderer.AddUIControl( productCode );
		productCode.setOnItemSelectedListener( new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
			{
				productName.selectItem( position );
				if ( position != 0 )
				{
					m_computedPrice = FetchPrice( position - 1 );
					price.SetText( m_computedPrice );
					try
					{
						Helper.SetKeyValue( orderData, "Part__c", products.getJSONArray( position ).getString( 0 ) );
						UOM.SetText( products.getJSONArray( position - 1 ).getString( 3 ) );
					}
					catch ( JSONException e )
					{
						e.printStackTrace();
					}
					selectedProduct = true;
				}
			}

			@Override
			public void onNothingSelected( AdapterView<?> arg0 )
			{
			}
		} );
		productCode.SetData( GetList( products, 2 ) );

		productName = new SpinnerInputField( getActivity().getBaseContext(), "Product Name:", R.layout.dropdown );
		productName.SetData( new ArrayList<String>() );
		SpinnerInputField.Bind( productName, displayData, getString( R.string.ORDER_ITEM_PRODUCT_NAME ) );
		renderer.AddUIControl( productName );
		productName.setOnItemSelectedListener( new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
			{
				productCode.selectItem( position );
			}

			@Override
			public void onNothingSelected( AdapterView<?> arg0 )
			{
			}
		} );
		productName.SetData( GetList( products, 1 ) );

		priceBook = new SpinnerInputField( getActivity().getBaseContext(), "Price Book:", R.layout.dropdown );
		priceBook.SetData( new ArrayList<String>() );
		SpinnerInputField.Bind( priceBook, displayData, getString( R.string.ORDER_ITEM_PRICE_BOOK ) );
		renderer.AddUIControl( priceBook );
		priceBook.setOnItemSelectedListener( new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
			{
			}

			@Override
			public void onNothingSelected( AdapterView<?> arg0 )
			{
			}
		} );
		priceBook.SetData( GetList( priceBooks, 1 ) );
		if ( priceBooks.length() >= 1 )
			priceBook.selectItem( 1 );

		price = new LabelTextField( getActivity().getBaseContext(), "Price:", R.layout.text_label );
		InputField.Bind( price, displayData, getString( R.string.ORDER_ITEM_PRICE ) );
		renderer.AddUIControl( price );

		quantity = new TextInputField( getActivity().getBaseContext(), "Quantity:", R.layout.edit_text );
		InputField.Bind( quantity, displayData, getString( R.string.ORDER_ITEM_QUANTITY ) );
		renderer.AddUIControl( quantity );
		quantity.setInputType( InputType.TYPE_CLASS_NUMBER );
		quantity.setOnTextChangeListener( new TextWatcher()
		{
			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count )
			{
				if ( s.length() > 0 )
				{
					computeTotalPrice();
					Helper.SetKeyValue( orderData, "Quantity__c", s.toString() );
					selectedQuantity = true;
				}
				else
					resetTotalPrice();
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after )
			{
			}

			@Override
			public void afterTextChanged( Editable s )
			{
			}
		} );

		UOM = new LabelTextField( getActivity().getBaseContext(), "UOM:", R.layout.text_label );
		InputField.Bind( UOM, orderData, "UOM__c" );
		InputField.Bind( UOM, displayData, getString( R.string.ORDER_ITEM_UOM ) );
		renderer.AddUIControl( UOM );

		discount = new TextInputField( getActivity().getBaseContext(), "Discount:", R.layout.edit_text );
		InputField.Bind( discount, orderData, "Discount__c" );
		InputField.Bind( discount, displayData, getString( R.string.ORDER_ITEM_DISCOUNT ) );
		renderer.AddUIControl( discount );
		discount.setInputType( InputType.TYPE_CLASS_NUMBER );
		discount.setOnTextChangeListener( new TextWatcher()
		{
			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count )
			{
				computeTotalPrice();
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after )
			{
			}

			@Override
			public void afterTextChanged( Editable s )
			{
			}
		} );

		salesPrice = new LabelTextField( getActivity().getBaseContext(), "Sales Price:", R.layout.text_label );
		InputField.Bind( salesPrice, orderData, "Sales_Price__c" );
		InputField.Bind( salesPrice, displayData, getString( R.string.ORDER_ITEM_SALES_PRICE ) );
		renderer.AddUIControl( salesPrice );

		netPrice = new LabelTextField( getActivity().getBaseContext(), "Net Price:", R.layout.text_label );
		InputField.Bind( netPrice, orderData, "Net_Price__c" );
		InputField.Bind( netPrice, displayData, getString( R.string.ORDER_ITEM_NET_PRICE ) );
		renderer.AddUIControl( netPrice );

		renderer.Render();
	}

	private void computeTotalPrice()
	{
		double qty = 0;
		if ( quantity.GetValue() != null && quantity.GetValue().length() != 0 )
		{
			qty = Double.parseDouble( quantity.GetValue() );
		}
		if ( qty == 0 )
			return;
		double dsc = 0;
		if ( discount.GetValue() != null )
		{
			if( discount.GetValue().length() != 0 )
				dsc = Double.parseDouble( discount.GetValue() );
		}

		double salesprice = Double.parseDouble( m_computedPrice ) - dsc;
		double netprice = salesprice * qty;

		salesPrice.SetText( Double.toString( salesprice ) );
		netPrice.SetText( Double.toString( netprice ) );
	}

	private void resetTotalPrice()
	{
		salesPrice.SetText( Double.toString( 0.0 ) );
		netPrice.SetText( Double.toString( 0.0 ) );
	}

	private ArrayList<String> GetList( JSONArray obj, int index )
	{
		ArrayList<String> retval = new ArrayList<>();
		retval.add( "Select..." );

		int count = obj.length();

		for ( int i = 0; i < count; i++ )
		{
			try
			{
				retval.add( obj.getJSONArray( i ).getString( index ) );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
		}
		return retval;
	}

	public void Query()
	{
		SmartStore store = SFApp.GetSmartStore();
		try
		{
			String query = "select {PricebookEntry:Id}, {PricebookEntry:Name}, {PricebookEntry:Pricebook2Id}, {PricebookEntry:Product2Id}, {PricebookEntry:ProductCode}, {PricebookEntry:UnitPrice} from {PricebookEntry} where {PricebookEntry:Pricebook2Id}='" + m_selectedPriceBookId + "'";
			priceBookEntries = store.query( QuerySpec.buildSmartQuerySpec( query, 500 ), 0 );

			StringBuilder priceBookIds = new StringBuilder();
			StringBuilder productIds = new StringBuilder();
			int count = priceBookEntries.length();

			for ( int i = 0; i < count; i++ )
			{
				JSONArray arr = priceBookEntries.getJSONArray( i );
				if ( i == count - 1 )
				{
					priceBookIds.append( "'" + arr.getString( 2 ) + "'" );
					productIds.append( "'" + arr.getString( 3 ) + "'" );
					continue;
				}
				priceBookIds.append( "'" + arr.getString( 2 ) + "'," );
				productIds.append( "'" + arr.getString( 3 ) + "'," );
			}

			query = "select {Pricebook2:Id}, {Pricebook2:Name} from {Pricebook2} where {Pricebook2:Id} IN (" + priceBookIds.toString() + ")";
			priceBooks = store.query( QuerySpec.buildSmartQuerySpec( query, 500 ), 0 );

			query = "select {Product2:Id}, {Product2:Name}, {Product2:ProductCode}, {Product2:UOM__c} from {Product2} where {Product2:Id} IN (" + productIds.toString() + ")";
			products = store.query( QuerySpec.buildSmartQuerySpec( query, 500 ), 0 );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public String FetchPrice( int position )
	{
		String price = null;
		try
		{
			String productId = products.getJSONArray( position ).getString( 0 );

			int count = priceBookEntries.length();

			for ( int i = 0; i < count; i++ )
			{
				if ( priceBookEntries.getJSONArray( i ).getString( 3 ).equals( productId ) )
				{
					price = priceBookEntries.getJSONArray( i ).getString( 5 );
					Helper.SetKeyValue( orderData, "Unit_Price__c", price );
					break;
				}
			}
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		return price;
	}
}
