package com.extentor.sfandroid.fragments;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import com.extentor.helper.Helper;
import com.extentor.helper.SoupHelper;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfandroid.activity.BaseFragment;
import com.extentor.sfandroid.activity.Home;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

public class HomeFragment extends BaseFragment
{
	ProgressDialog	pd	= null;
	EditText		edit1;
	ImageView		navigation;
	ListView		listView;
	int				selectedDay;

	LinearLayout	relativeLayoutObjectCountValidation;
	Button			buttonSync;

	public HomeFragment()
	{
	}

	@Override
	public void setArguments( Bundle args )
	{
		super.setArguments( args );
	}

	public void Update()
	{
		View view = this.getView();
		TextView name = (TextView) view.findViewById( R.id.name );
		name.setText( Helper.GetSharedPrefItem( "name" ) );
		TextView email = (TextView) view.findViewById( R.id.email );
		email.setText( Helper.GetSharedPrefItem( "email" ) );

		relativeLayoutObjectCountValidation = (LinearLayout) view.findViewById( R.id.relativeLayoutObjectCountValidation );
		buttonSync = (Button) view.findViewById( R.id.buttonSync );

		buttonSync.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				Home home = (Home) getActivity();
				home.fireSync();
			}
		} );
	}

	public void ClearErrors()
	{
		relativeLayoutObjectCountValidation.removeAllViews();
		buttonSync.setVisibility( View.GONE );
	}
	
	private void AddTextView( String message, int color )
	{
//		TextView textView = new TextView( getActivity() );
//		textView.setText( message );
//		textView.setTextColor( color );
//		LayoutParams layoutParams = new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
//		textView.setLayoutParams( layoutParams );
//		relativeLayoutObjectCountValidation.addView( textView );
	}

	private void AddObjectCountReport( String soupName, int count )
	{
		String message = "";
		int color = 0;
		if ( count == 0 )
		{
			message = String.format( "Object error: %s [0 Records]", Helper.ProcessKey( soupName ) );
			color = Color.RED;
		}
		else
		{
			message = String.format( "Object: %s [%d Records]", Helper.ProcessKey( soupName ), count );
			color = Color.parseColor( getActivity().getResources().getString( R.color.header_primary_blue ) );
		}
		
		AddTextView( message, color );
	}

	public void UpdateErrors()
	{
		List<String> m_Soups = SoupHelper.GetAllSoupNames();
		m_Soups.remove( "syncs_soup" );

		if ( m_Soups.size() == 0 )
		{
			AlertDialog.Builder alertDialog = new AlertDialog.Builder( null );
			alertDialog.setTitle( "Alert!" );
			alertDialog.setMessage( "Application error! Please re-login." );
			Dialog alert = alertDialog.create();
			alert.show();
			return;
		}

		SmartStore store = SFApp.GetSmartStore();
		Boolean flag = false;
		try
		{
			for ( String soupName : m_Soups )
			{
				JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select Count(*) from {" + soupName + "}", 10 ), 0 );
				int recordCount = result.getJSONArray( 0 ).getInt( 0 );
				if ( recordCount == 0 )
					flag = true;
				AddObjectCountReport( soupName, recordCount );
			}

			if ( flag )
			{
				buttonSync.setVisibility( View.VISIBLE );
				AddTextView( "Errors found! Please resync or relogin!", 0 );
			}
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
			AlertDialog.Builder alertDialog = new AlertDialog.Builder( null );
			alertDialog.setTitle( "Alert!" );
			alertDialog.setMessage( "Application error! Please re-login." );
			Dialog alert = alertDialog.create();
			alert.show();
			return;
		}
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		View view = inflater.inflate( R.layout.home_fragment, null );
		TextView name = (TextView) view.findViewById( R.id.name );
		name.setText( Helper.GetSharedPrefItem( "name" ) );
		TextView email = (TextView) view.findViewById( R.id.email );
		email.setText( Helper.GetSharedPrefItem( "email" ) );
		return view;
	}

	@Override
	public void onActivityCreated( Bundle savedInstanceState )
	{
		super.onActivityCreated( savedInstanceState );
	}
}
