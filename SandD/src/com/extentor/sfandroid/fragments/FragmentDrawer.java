package com.extentor.sfandroid.fragments;

/**
 * Created by Ankur on 7/30/2015.
 */

import java.util.ArrayList;
import java.util.List;
import com.extentor.custom.base.Model.NavDrawerItem;
import com.extentor.helper.Helper;
import com.extentor.interfaces.IFragmentDrawerListener;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.activity.BaseFragment;
import com.extentor.sfandroid.activity.Home;
import com.extentor.sfandroid.adapters.NavigationDrawerAdapter;

public class FragmentDrawer extends BaseFragment
{
	private ListView				recyclerView;
	private NavigationDrawerAdapter	adapter;
	private static String[]			titles	= null;
	private IFragmentDrawerListener	drawerListener;

	public FragmentDrawer()
	{

	}

	public void setDrawerListener( IFragmentDrawerListener listener )
	{
		this.drawerListener = listener;
	}

	public static List<NavDrawerItem> getData()
	{
		List<NavDrawerItem> data = new ArrayList<NavDrawerItem>();
		// preparing navigation drawer items
		for ( int i = 0; i < titles.length; i++ )
		{
			NavDrawerItem navItem = new NavDrawerItem();
			navItem.setTitle( titles[i] );
			data.add( navItem );
		}
		return data;
	}
	
	public void Update()
	{
		View view = this.getView();
		TextView name = (TextView) view.findViewById( R.id.name );
		name.setText( Helper.GetSharedPrefItem( "name" ) );
		TextView email = (TextView) view.findViewById( R.id.email );
		email.setText( Helper.GetSharedPrefItem( "email" ) );
	}

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );

		// drawer labels
		titles = getActivity().getResources().getStringArray( R.array.nav_drawer_labels );
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		// Inflating view layout
		View layout = inflater.inflate( R.layout.fragment_nav_drawer, container, false );
		recyclerView = (ListView) layout.findViewById( R.id.drawerList );
		
		TextView name = (TextView) layout.findViewById( R.id.name );
		name.setText( Helper.GetSharedPrefItem( "name" ) );
		TextView email = (TextView) layout.findViewById( R.id.email );
		email.setText( Helper.GetSharedPrefItem( "email" ) );

		adapter = new NavigationDrawerAdapter( getActivity(), getData() );
		recyclerView.setAdapter( adapter );
		// mDrawerLayout.closeDrawer(containerView);

		recyclerView.setOnItemClickListener( new OnItemClickListener()
		{
			@Override
			public void onItemClick( AdapterView<?> parent, View view, int position, long id )
			{
				drawerListener.onDrawerItemSelected( view, position );
//				mDrawerLayout.closeDrawer( containerView );
			}
		} );
		
		Button buttonLogout = (Button) layout.findViewById( R.id.buttonLogout );
		buttonLogout.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				Home activity = (Home)getActivity();
				activity.performLogout();
			}
		} );

		return layout;
	}

	public void setUp( int fragmentId, DrawerLayout drawerLayout )
	{
	}

}