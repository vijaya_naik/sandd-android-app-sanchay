package com.extentor.sfandroid.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;

public class DateInputField extends InputField implements OnClickListener, OnDateSetListener
{
	private EditText			m_editText;
	private SimpleDateFormat	m_dateFormat;
	private DatePickerDialog	m_dateDialog;
	private OnDateSetListener	m_listener;

	public DateInputField( Context context, String label )
	{
		super( context, label );
		m_editText = new EditText( m_context );
	}

	public DateInputField( Context context, String label, int resId )
	{
		super( context, label, resId );
		m_editText = (EditText) Create( context, resId );
	}

	public DateInputField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		super( context, label, marginLeft, marginTop, padding );
		m_editText = new EditText( m_context );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		m_editText.setInputType( InputType.TYPE_NULL );

		m_editText.setOnClickListener( this );

		m_container.addView( m_editText );

		Calendar calendar = Calendar.getInstance();

		if ( m_dateFormat == null )
		{
			m_dateFormat = new SimpleDateFormat( "dd-MM-yyyy" );
		}

		m_dateDialog = new DatePickerDialog( m_context, this, calendar.get( Calendar.YEAR ), calendar.get( Calendar.MONTH ), calendar.get( Calendar.DAY_OF_MONTH ) );
		m_dateDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

		super.Draw( parent );
	}

	@Override
	public void SetLayoutParam( LayoutParams params )
	{
		m_uiLayoutParams = params;
	}

	@Override
	public void onClick( View v )
	{
		m_dateDialog.show();
	}
	
	public void setOnDateSetListener( OnDateSetListener listener )
	{
		m_listener = listener;
	}

	@Override
	public void onDateSet( DatePicker view, int year, int monthOfYear, int dayOfMonth )
	{
		// TODO Auto-generated method stub
		Calendar calendar = Calendar.getInstance();
		calendar.set( year, monthOfYear, dayOfMonth );

		String date = m_dateFormat.format( calendar.getTime() );
		m_value = date;
		
		if( m_inputs != null )
		{
			Update();
			return;
		}

		Iterator<String> keys = m_binder.keySet().iterator();
		while( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, date );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
		
		m_editText.setText( date );
		if( m_listener != null )
		{
			m_listener.onDateSet( view, year, monthOfYear, dayOfMonth );
		}
	}

	public void setDateFormat( String format )
	{
		m_dateFormat = null;
		m_dateFormat = new SimpleDateFormat( format );
	}
	
	@Override
	public void SetValue()
	{
		m_editText.setText( m_value );
	}
}
