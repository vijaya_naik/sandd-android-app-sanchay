package com.extentor.sfandroid.ui;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class LabelTextField extends InputField
{
	private TextView	m_textView;
	private String		m_value;

	public LabelTextField( Context context, String label )
	{
		super( context, null );
		m_textView = new TextView( context );
		m_value = label;
	}

	public LabelTextField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		super( context, label, marginLeft, marginTop, padding );
		m_textView = new TextView( context );
		m_value = label;
	}

	public LabelTextField( Context context, String label, int resId )
	{
		super( context, null, resId );
		m_textView = (TextView) Create( context, resId );
		m_value = label;
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		m_textView.setLayoutParams( m_uiLayoutParams );
		m_textView.setText( m_value );

		m_container.addView( m_textView );

		super.Draw( parent );
	}

	@Override
	public void SetLayoutParam( LayoutParams params )
	{
		m_uiLayoutParams = params;
	}

	@Override
	public void SetValue()
	{
		m_textView.setText( m_value );
	}

	public void SetText( String text )
	{
		m_textView.setText( m_value + " " + text );
		Iterator<String> keys = m_binder.keySet().iterator();
		while( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, text );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
		}
	}
}
