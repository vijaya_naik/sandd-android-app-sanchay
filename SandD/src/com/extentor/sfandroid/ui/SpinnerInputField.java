package com.extentor.sfandroid.ui;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.sfandroid.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SpinnerInputField extends InputField implements OnItemSelectedListener
{
	private Spinner					m_spinner;
	private ArrayList<String>		m_spinnerData;

	public Boolean					isMultiSelect;

	private int						m_position;
	private OnItemSelectedListener	m_listener;

	public SpinnerInputField( Context context, String label )
	{
		super( context, label );
		m_spinner = new Spinner( m_context );
		isMultiSelect = false;
	}

	public SpinnerInputField( Context context, String label, int resId )
	{
		super( context, label, resId );
		m_spinner = (Spinner) Create( context, resId );
		isMultiSelect = false;
	}

	public SpinnerInputField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		super( context, label, marginLeft, marginTop, padding );
		m_spinner = new Spinner( m_context );
		isMultiSelect = false;
	}

	public void UpdateData( ArrayList<String> data )
	{
		m_spinnerData = data;
		ArrayAdapter<String> adapter = new ArrayAdapter<String>( m_context, R.layout.spinner_item, m_spinnerData );
		m_spinner.setAdapter( adapter );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		m_spinner.setLayoutParams( m_uiLayoutParams );

		m_container.addView( m_spinner );

		ArrayAdapter<String> adapter = new ArrayAdapter<String>( m_context, R.layout.spinner_item, m_spinnerData );

		m_spinner.setAdapter( adapter );
		m_spinner.setOnItemSelectedListener( this );

		super.Draw( parent );
	}

	@Override
	public void SetLayoutParam( LayoutParams params )
	{
		m_uiLayoutParams = params;
	}

	public void SetData( ArrayList<String> data )
	{
		m_spinnerData = data;
	}

	@Override
	public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
	{
		m_spinner.setSelection( position );
		if ( m_inputs != null )
		{
			Update();
			return;
		}

		String text = (String) m_spinner.getSelectedItem();
		m_value = text;
		m_position = position;
		if ( text.equals( NONE ) )
		{
			text = "";
		}

		Iterator<String> keys = m_binder.keySet().iterator();
		while ( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, text );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch ( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
		if ( m_listener != null )
			m_listener.onItemSelected( parent, view, position, id );

	}

	public void setOnItemSelectedListener( OnItemSelectedListener listener )
	{
		m_listener = listener;
	}

	public void selectItem( int position )
	{
		m_spinner.setSelection( position );
	}

	@Override
	public void onNothingSelected( AdapterView<?> parent )
	{
	}

	@Override
	public void SetValue()
	{
		m_spinner.setSelection( m_position );
	}
}
