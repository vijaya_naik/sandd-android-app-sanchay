package com.extentor.sfandroid.ui;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;

public class TextAreaInputField extends InputField implements TextWatcher
{
	private EditText	m_textArea;

	public TextAreaInputField( Context context, String label )
	{
		super( context, label );
		m_textArea = new EditText( m_context );
	}

	public TextAreaInputField( Context context, String label, int resId )
	{
		super( context, label, resId );
		m_textArea = (EditText) Create( context, resId );
	}

	public TextAreaInputField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		super( context, label, marginLeft, marginTop, padding );
		m_textArea = new EditText( m_context );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		m_textArea.setEms( 10 );
		m_textArea.setInputType( InputType.TYPE_TEXT_FLAG_MULTI_LINE );

//		android.widget.LinearLayout.LayoutParams layoutParams = new LayoutParams( m_uiLayoutParams );
//		layoutParams.gravity = Gravity.LEFT | Gravity.TOP;

		m_container.addView( m_textArea );

		super.Draw( parent );
	}

	@Override
	public void SetLayoutParam( LayoutParams params )
	{
		m_uiLayoutParams = params;
	}

	@Override
	public void beforeTextChanged( CharSequence s, int start, int count, int after )
	{
	}

	@Override
	public void onTextChanged( CharSequence s, int start, int before, int count )
	{
		m_value = (String) s;
		if ( m_inputs != null )
		{
			Update();
			return;
		}
		Iterator<String> keys = m_binder.keySet().iterator();
		while ( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, m_value );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch ( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void afterTextChanged( Editable s )
	{
	}
	
	@Override
	public void SetValue()
	{
		m_textArea.setText( m_value );
	}
}
