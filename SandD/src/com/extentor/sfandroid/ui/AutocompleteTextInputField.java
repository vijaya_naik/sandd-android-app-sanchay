package com.extentor.sfandroid.ui;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.sfandroid.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class AutocompleteTextInputField extends InputField implements OnItemSelectedListener, OnItemClickListener
{
	private AutoCompleteTextView	m_autoComplete;
	private ArrayList<String>		m_spinnerData;
	private OnItemSelectedListener	m_listener;

	public AutocompleteTextInputField( Context context, String label )
	{
		super( context, label );
		// TODO Auto-generated constructor stub
		m_autoComplete = new AutoCompleteTextView( m_context );
	}

	public AutocompleteTextInputField( Context context, String label, int resId )
	{
		super( context, label, resId );
		// TODO Auto-generated constructor stub
		m_autoComplete = (AutoCompleteTextView) Create( context, resId );
	}

	public AutocompleteTextInputField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		super( context, label, marginLeft, marginTop, padding );
		m_autoComplete = new AutoCompleteTextView( m_context );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		// TODO Auto-generated method stub
		m_autoComplete.setLayoutParams( m_uiLayoutParams );

		m_container.addView( m_autoComplete );

		ArrayAdapter<String> adapter = new ArrayAdapter<String>( m_context, R.layout.spinner_item, m_spinnerData );

		m_autoComplete.setAdapter( adapter );
		m_autoComplete.setOnItemClickListener( this );

		super.Draw( parent );
	}

	@Override
	public void SetLayoutParam( LayoutParams params )
	{
		m_uiLayoutParams = params;
	}

	public void SetData( ArrayList<String> data )
	{
		m_spinnerData = data;
	}

	@Override
	public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
	{
		m_value = m_autoComplete.getText().toString();
		if ( m_inputs != null )
		{
			Update();
			return;
		}

		Iterator<String> keys = m_binder.keySet().iterator();
		while ( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, m_value );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch ( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onNothingSelected( AdapterView<?> parent )
	{
	}

	@Override
	public void onItemClick( AdapterView<?> parent, View view, int position, long id )
	{
		// TODO Auto-generated method stub
		m_value = m_autoComplete.getText().toString();
		if ( m_inputs != null )
		{
			Update();
			return;
		}
		Iterator<String> keys = m_binder.keySet().iterator();
		while ( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, m_value );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch ( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
		if ( m_listener != null )
			m_listener.onItemSelected( parent, view, position, id );
	}

	@Override
	public void SetValue()
	{
		m_autoComplete.setText( m_value );
	}
	
	public void setOnItemSelectedListener( OnItemSelectedListener listener )
	{
		m_listener = listener;
	}
}
