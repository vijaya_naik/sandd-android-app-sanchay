package com.extentor.sfandroid.ui;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SectionHeader extends InputField
{
	private TextView	m_sectionLabel;

	public SectionHeader( Context context, String label, int resId )
	{
		super( context, label, resId );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		m_containerLayoutParams = new android.widget.LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT );

		m_container = new LinearLayout( m_context );
		m_container.setLayoutParams( m_containerLayoutParams );
		m_container.setPadding( 0, 2, 0, 2 );
		m_container.setOrientation( LinearLayout.VERTICAL );
		m_container.setBackgroundColor( 0xFFE3DAC9 );
		
		m_sectionLabel = (TextView) Create( m_context, m_resId );
		m_sectionLabel.setText( m_labelText );
		m_value = m_labelText;
		
		m_container.addView( m_sectionLabel );

		parent.addView( m_container );
	}
	
	@Override
	public void SetLayoutParam( android.widget.LinearLayout.LayoutParams params )
	{
	}
	
	@Override
	public void SetValue()
	{
		m_sectionLabel.setText( m_value );
	}
}
