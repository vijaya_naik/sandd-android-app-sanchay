package com.extentor.sfandroid.ui;

import java.util.ArrayList;

import com.extentor.interfaces.IInputField;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;

public class UIRenderer
{
	private ArrayList<IInputField>	m_inputList;
	private ViewGroup				m_viewGroup;
	private int						m_index		= 0;
	private Message					m_message	= null;
	private IRenderer				m_renderer = null;

	public UIRenderer( ViewGroup viewGroup )
	{
		// TODO Auto-generated constructor stub
		m_inputList = new ArrayList<IInputField>();
		m_viewGroup = viewGroup;
		m_viewGroup.setEnabled( false );
	}

	public void AddUIControl( IInputField control )
	{
		m_inputList.add( control );
	}

	public void Render()
	{
		m_message = m_handler.obtainMessage();
		m_message.arg1 = 0;
		m_message.arg2 = m_index;
		m_handler.sendMessage( m_message );
	}
	
	public void Render( IRenderer renderer )
	{
		m_renderer = renderer;
		Render();
	}

	public void RenderValues()
	{
		m_message = m_handler.obtainMessage();
		m_index = 0;
		m_message.arg1 = 1;
		m_message.obj = m_inputList.get( m_index );
		m_handler.sendMessage( m_message );
	}

	private Handler	m_handler	= new Handler()
	{
		@Override
		public void handleMessage( Message msg )
		{
			if ( msg.arg1 == 0 )// render controls
			{
				if ( m_index == m_inputList.size() )
				{
					m_viewGroup.setEnabled( true );
					m_index = 0;
					if( m_renderer != null )
					{
						m_renderer.RenderingDone();
					}
					return;
				}
				IInputField obj = m_inputList.get( m_index );
				obj.Draw( m_viewGroup );
				m_index++;
				Render();
			}
			else
			// render values
			{
				IInputField input = (IInputField) msg.obj;
				input.SetValue();
				m_index++;
				RenderValues();
			}
		}
	};
}
