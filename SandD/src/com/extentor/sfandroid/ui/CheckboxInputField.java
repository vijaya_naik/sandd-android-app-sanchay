package com.extentor.sfandroid.ui;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout.LayoutParams;

public class CheckboxInputField extends InputField implements OnCheckedChangeListener
{
	private CheckBox	m_checkBox;
	private Boolean		m_checked;

	public CheckboxInputField( Context context, String label )
	{
		super( context, label );
		m_checkBox = new CheckBox( m_context );
	}

	public CheckboxInputField( Context context, String label, int resId )
	{
		super( context, label, resId );
		m_checkBox = (CheckBox) Create( context, resId );
	}

	public CheckboxInputField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		super( context, label, marginLeft, marginTop, padding );
		m_checkBox = new CheckBox( m_context );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		m_uiLayoutParams = new android.widget.LinearLayout.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
		m_checkBox.setLayoutParams( m_uiLayoutParams );

		m_checkBox.setOnCheckedChangeListener( this );

		m_containerLabel.addView( m_checkBox );

		super.Draw( parent );
	}

	@Override
	public void SetLayoutParam( LayoutParams params )
	{
		m_uiLayoutParams = params;
	}

	@Override
	public void onCheckedChanged( CompoundButton buttonView, boolean isChecked )
	{
		Iterator<String> keys = m_binder.keySet().iterator();
		while( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, isChecked );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void SetValue()
	{
		m_checkBox.setChecked( m_checked );
	}
}
