package com.extentor.sfandroid.ui;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.extentor.interfaces.IInputField;

public abstract class InputField implements IInputField
{
	protected String						m_labelText;
	protected Context						m_context;

	protected LinearLayout					m_container;
	protected LinearLayout					m_containerLabel;

	protected LinearLayout.LayoutParams		m_containerLayoutParams;
	protected LinearLayout.LayoutParams		m_uiLayoutParams;

	protected TextView						m_label;
	protected int							m_resId;
	protected View							m_View;

	protected int							m_marginLeft	= 10;
	protected int							m_marginTop		= 3;
	protected int							m_padding		= 10;

	public Boolean							required		= false;

	protected HashMap<String, JSONObject>	m_binder;

	public static String					NONE			= "--NONE--";

	protected String						m_value			= "";
	protected ArrayList<InputField>			m_inputs;

	protected Object Create( Context context, String className )
	{
		Class<?> myClass = null;
		try
		{
			myClass = Class.forName( className );
		}
		catch ( ClassNotFoundException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Constructor<?> myConstructor = null;
		try
		{
			myConstructor = myClass.getConstructor( String.class );
		}
		catch ( NoSuchMethodException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			return myConstructor.newInstance( new Object[]{context} );
		}
		catch ( InstantiationException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch ( IllegalAccessException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch ( IllegalArgumentException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch ( InvocationTargetException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return myConstructor;
	}

	protected Object Create( Context context, int resId )
	{
		LayoutInflater inflater = (LayoutInflater) m_context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		return inflater.inflate( resId, null );
	}

	public InputField( Context context, String label )
	{
		// TODO Auto-generated constructor stub
		m_context = context;
		m_labelText = label;

		required = false;

		m_resId = 0;

		Setup();
	}

	public InputField( Context context, String label, int resId )
	{
		m_context = context;
		m_labelText = label;

		required = false;

		m_resId = resId;

		Setup();
	}

	public InputField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		this( context, label );

		m_marginLeft = marginLeft;
		m_marginTop = marginTop;
		m_padding = padding;

		required = false;

		m_resId = 0;

		Setup();
	}

	private void Setup()
	{
		m_containerLayoutParams = new android.widget.LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT );
		m_containerLayoutParams.setMargins( m_marginLeft, m_marginTop, m_marginLeft, m_marginTop );

		m_container = new LinearLayout( m_context );
		m_container.setLayoutParams( m_containerLayoutParams );
		m_container.setPadding( m_padding, m_padding, m_padding, m_padding );
		m_container.setOrientation( LinearLayout.VERTICAL );

		m_uiLayoutParams = new android.widget.LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT );

		android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
		params.setMargins( m_marginLeft, m_marginTop, m_marginLeft, m_marginTop );

		m_containerLabel = new LinearLayout( m_context );
		m_containerLabel.setLayoutParams( params );
		m_containerLabel.setOrientation( LinearLayout.HORIZONTAL );

		if ( m_labelText != null )
		{
			m_label = new TextView( m_context );
			m_label.setLayoutParams( m_uiLayoutParams );
			m_label.setTextColor( Color.BLACK );
			m_label.setText( m_labelText );

			m_containerLabel.addView( m_label );
		}

		m_container.addView( m_containerLabel );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		parent.addView( m_container );
	}

	@Override
	public void Bind( JSONObject obj, String key )
	{
		if( m_binder == null )
		{
			m_binder = new HashMap<String, JSONObject>();
		}
		m_binder.put( key, obj );
	}

	public static void Bind( InputField input, JSONObject obj, String key )
	{
		input.Bind( obj, key );
	}

	/**
	 * Provides the ability to 'link' several input fields together
	 * 
	 * @param obj
	 * @param key
	 * @param inputs
	 */
//	public void Link( JSONObject obj, String key, InputField... inputs )
//	{
//		m_binder.add( obj );
//		m_binderKey = key;
//		if ( m_inputs == null )
//		{
//			m_inputs = new ArrayList<InputField>();
//			m_inputs.add( this );
//			for ( InputField input : inputs )
//			{
//				m_inputs.add( input );
//			}
//		}
//	}

	protected void Update()
	{
		StringBuilder strBuilder = new StringBuilder();
		for ( InputField input : m_inputs )
		{
			strBuilder.append( input.GetValue() + " " );
		}
		String temp = strBuilder.toString().trim();
		Iterator<String> keys = m_binder.keySet().iterator();
		while( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, temp );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public String GetValue()
	{
		return m_value;
	}

	public static void SetLayoutParams( Class<?> cls, LayoutParams param )
	{

	}
}
