package com.extentor.sfandroid.ui;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;

public class TextInputField extends InputField implements TextWatcher
{
	private EditText	m_editText;
	private String		m_value;
	private TextWatcher	m_listener;

	public TextInputField( Context context, String label )
	{
		super( context, label );
		m_editText = new EditText( context );
	}

	public TextInputField( Context context, String label, int resId )
	{
		super( context, label, resId );
		m_editText = (EditText) Create( context, resId );
	}

	public TextInputField( Context context, String label, int marginLeft, int marginTop, int padding )
	{
		super( context, label, marginLeft, marginTop, padding );
		m_editText = new EditText( context );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		m_editText.setLayoutParams( m_uiLayoutParams );

		m_editText.addTextChangedListener( this );

		m_container.addView( m_editText );

		super.Draw( parent );
	}

	public void SetLayoutParam( LayoutParams params )
	{
		m_uiLayoutParams = params;
	}

	@Override
	public void beforeTextChanged( CharSequence s, int start, int count, int after )
	{
	}

	@Override
	public void onTextChanged( CharSequence s, int start, int before, int count )
	{
		m_value = (String) s.toString();
		if ( m_inputs != null )
		{
			Update();
			return;
		}
		
		Iterator<String> keys = m_binder.keySet().iterator();
		while ( keys.hasNext() )
		{
			String key = keys.next();
			JSONObject obj = m_binder.get( key );
			try
			{
				obj.put( key, m_value );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			catch ( NullPointerException e )
			{
				e.printStackTrace();
			}
		}
		if ( m_listener != null )
		{
			m_listener.onTextChanged( s, start, before, count );
		}
	}

	@Override
	public void afterTextChanged( Editable s )
	{
	}

	@Override
	public String GetValue()
	{
		return m_value;
	}

	@Override
	public void SetValue()
	{
		m_editText.setText( m_value );
	}

	public void setOnTextChangeListener( TextWatcher listener )
	{
		m_listener = listener;
	}

	public void setInputType( int type )
	{
		m_editText.setRawInputType( type );
	}
	
	public void setText( String s )
	{
		m_editText.setText( s );
	}
}