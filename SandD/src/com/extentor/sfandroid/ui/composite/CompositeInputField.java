package com.extentor.sfandroid.ui.composite;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.extentor.interfaces.IInputField;

public abstract class CompositeInputField implements IInputField
{
	protected String					m_labelText;
	protected Context					m_context;

	protected LinearLayout				m_container;
	protected LinearLayout				m_containerLabel;

	protected LinearLayout.LayoutParams	m_containerLayoutParams;
	protected LinearLayout.LayoutParams	m_uiLayoutParams;

	protected TextView					m_label;
	protected int						m_layoutId;
	protected LinearLayout				m_layout;
	protected View						m_View;

	private int							m_margin	= 10;
	private int							m_padding	= 0;

	public Boolean						required	= false;

	protected JSONObject				m_binder;
	protected String					m_binderKey;

	public static String				NONE		= "--NONE--";

	protected Object Create( Context context, String className )
	{
		Class<?> myClass = null;
		try
		{
			myClass = Class.forName( className );
		}
		catch ( ClassNotFoundException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Constructor<?> myConstructor = null;
		try
		{
			myConstructor = myClass.getConstructor( String.class );
		}
		catch ( NoSuchMethodException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			return myConstructor.newInstance( new Object[]{context} );
		}
		catch ( InstantiationException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch ( IllegalAccessException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch ( IllegalArgumentException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch ( InvocationTargetException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return myConstructor;
	}

	protected Object Create( Context context, int resId )
	{
		LayoutInflater inflater = (LayoutInflater) m_context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		return inflater.inflate( resId, null );
	}

	public CompositeInputField( Context context, String label, int layoutId )
	{
		m_context = context;
		m_labelText = label;

		required = false;

		m_layoutId = layoutId;

		Setup();
	}

	private void Setup()
	{
		m_containerLayoutParams = new android.widget.LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT );
		m_containerLayoutParams.setMargins( m_margin, m_margin, m_margin, m_margin );

		m_container = new LinearLayout( m_context );
		m_container.setLayoutParams( m_containerLayoutParams );
		m_container.setPadding( m_padding, m_padding, m_padding, m_padding );
		m_container.setOrientation( LinearLayout.VERTICAL );

		m_uiLayoutParams = new android.widget.LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT );

		android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
		params.setMargins( m_margin, m_margin, m_padding, m_margin );

		m_containerLabel = new LinearLayout( m_context );
		m_containerLabel.setLayoutParams( params );
		m_containerLabel.setOrientation( LinearLayout.HORIZONTAL );

		m_label = new TextView( m_context );
		m_label.setLayoutParams( m_uiLayoutParams );
		m_label.setText( m_labelText );

		m_containerLabel.addView( m_label );

		m_container.addView( m_containerLabel );

		m_layout = (LinearLayout) Create( m_context, m_layoutId );
		
		m_container.addView( m_layout );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		// TODO Auto-generated method stub
		parent.addView( m_container );
	}

	@Override
	public void Bind( JSONObject obj, String key )
	{
		// TODO Auto-generated method stub
		m_binder = obj;
		m_binderKey = key;
	}

	public static void Bind( CompositeInputField input, JSONObject obj, String keys )
	{
		input.Bind( obj, keys );
	}
}
