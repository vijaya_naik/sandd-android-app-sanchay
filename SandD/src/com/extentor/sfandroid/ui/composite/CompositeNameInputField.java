package com.extentor.sfandroid.ui.composite;

import org.json.JSONException;

import com.extentor.sfandroid.R;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;

public class CompositeNameInputField extends CompositeInputField implements OnItemSelectedListener, TextWatcher
{
	private Spinner		m_spinner;
	private EditText	m_firstName;
	private EditText	m_lastName;

	private String[]	m_spinnerData;

	private int			m_position;
	private String		m_salutation	= "", m_fName = "", m_lName = "";

	public CompositeNameInputField( Context context, String label, int layoutId, int[] resIds )
	{
		super( context, label, layoutId );

		assert resIds.length == 0 : "Resource count cannot be 0 for CompositeNameInputField";
		assert resIds.length > 3 : "Resource count cannot be more than 3 for CompositeNameInputField";

		int id_salutation = resIds[0];
		int id_firstName = resIds[1];
		int id_lastName = resIds[2];

		m_spinner = (Spinner) m_layout.findViewById( id_salutation );
		m_firstName = (EditText) m_layout.findViewById( id_firstName );
		m_lastName = (EditText) m_layout.findViewById( id_lastName );
	}

	@Override
	public void Draw( ViewGroup parent )
	{
		ArrayAdapter<String> adapter = new ArrayAdapter<String>( m_context, R.layout.spinner_item, m_spinnerData );

		m_spinner.setAdapter( adapter );
		m_spinner.setOnItemSelectedListener( (OnItemSelectedListener) this );

		m_firstName.addTextChangedListener( this );
		m_lastName.addTextChangedListener( this );

		super.Draw( parent );
	}

	public void SetData( String[] data )
	{
		m_spinnerData = data;
	}

	@Override
	public void beforeTextChanged( CharSequence s, int start, int count, int after )
	{
	}

	@Override
	public void onTextChanged( CharSequence s, int start, int before, int count )
	{
		try
		{
			if ( m_firstName.hasFocus() )
			{
				m_fName = (String) s.toString();
			}
			if ( m_lastName.hasFocus() )
			{
				m_lName = (String) s.toString();
			}
			String name = m_salutation + " " + m_fName + " " + m_lName;
			m_binder.put( m_binderKey, name );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		catch ( NullPointerException e )
		{
			e.printStackTrace();
		}
	}

	@Override
	public void afterTextChanged( Editable s )
	{
	}

	@Override
	public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
	{
		m_spinner.setSelection( position );
		try
		{
			m_salutation = (String) m_spinner.getSelectedItem();
			if ( m_salutation.equals( NONE ) )
			{
				m_salutation = "";
			}
			String name = m_salutation + " " + m_fName + " " + m_lName;
			m_binder.put( m_binderKey, name );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		catch ( NullPointerException e )
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onNothingSelected( AdapterView<?> parent )
	{
	}

	@Override
	public void SetLayoutParam( LayoutParams params )
	{
		// TODO Auto-generated method stub
	}

	@Override
	public String GetValue()
	{
		return null;
	}

	@Override
	public void SetValue()
	{
		m_spinner.setSelection( m_position );
		m_firstName.setText( m_fName );
		m_lastName.setText( m_lName );
	}
}
