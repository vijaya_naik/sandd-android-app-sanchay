/*
 * Copyright (c) 2011, salesforce.com, inc.
 * All rights reserved.
 * Redistribution and use of this software in source and binary forms, with or
 * without modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * - Neither the name of salesforce.com, inc. nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission of salesforce.com, inc.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.extentor.sfandroid;

import android.app.Application;
import android.content.SharedPreferences;

import com.extentor.helper.Helper;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.activity.Home;
import com.extentor.push.PushNotifications;
import com.salesforce.androidsdk.smartstore.store.SmartStore;
import com.salesforce.androidsdk.smartsync.app.SmartSyncSDKManager;

/**
 * Application class for our application.
 */
public class SFApp extends Application
{
	public PushNotifications			m_PushNotificationsInterface	= null;
	public static SmartSyncSDKManager	m_SmartSyncManager				= null;
	public static SmartStore			m_SmartStore					= null;
	public SharedPreferences			sharedPrefs						= null;

	@Override
	public void onCreate()
	{
		super.onCreate();

		FontsOverride.setDefaultFont( this, "DEFAULT", "proximanovasoft.ttf" );
		FontsOverride.setDefaultFont( this, "MONOSPACE", "proximanovasoft.ttf" );
		FontsOverride.setDefaultFont( this, "SERIF", "proximanovasoft.ttf" );
		FontsOverride.setDefaultFont( this, "SANS_SERIF", "proximanovasoft.ttf" );

		// initialize helper class
		Helper.SetApplication( this );

		SmartSyncSDKManager.initNative( getApplicationContext(), new KeyImpl(), Home.class );

		sharedPrefs = this.getSharedPreferences( getString( R.string.APP_SHARED_PREFS ), 0 );
	}

	public static SmartSyncSDKManager GetSmartSyncManager()
	{
		return m_SmartSyncManager;
	}

	public static SmartStore GetSmartStore()
	{
		return m_SmartStore;
	}
}
