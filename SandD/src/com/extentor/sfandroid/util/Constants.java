package com.extentor.sfandroid.util;

public interface Constants {

	public static final String STR_TRADE = "TRADE";
	public static final String STR_NON_TRADE = "NONTRADE";
	public static final String STR_SCHME = "SCHME";
	public static final String STR_OPC = "OPC";
	public static final String STR_PPC = "PPC";
	public static final String STR_PSC = "PSC";
	public static final String STR_ERROR = "Error";
	public static final String STR_INFO = "Information";
	public static final String STR_CLOSE = "Close";

	public static enum week {
		MON, TUE, WED, THU, FRI, SAT
	}

	// sp constants
	public static final String SP_DAY = "SelectedDay";
}
