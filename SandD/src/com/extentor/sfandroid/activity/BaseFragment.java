package com.extentor.sfandroid.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.extentor.sfandroid.R;
import com.extentor.sfandroid.fragments.HomeFragment;
import com.extentor.sfandroid.util.Constants;

public class BaseFragment extends Fragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	protected void setPageTitle(int resId) {
		((TextView) getActivity().findViewById(R.id.title))
				.setText(getString(resId));

	}

	protected void setPageTitle(String title) {
		((TextView) getActivity().findViewById(R.id.title)).setText(title);

	}
	
	protected void navigateToClass(Class<?> classTo)
	{
		startActivity(new Intent(getActivity().getBaseContext(), classTo));
		getActivity().overridePendingTransition( R.anim.slide_in, R.anim.none );
	}

	protected void gotoClass(Class<?> classTo) {
		startActivity(new Intent(getActivity().getBaseContext(), classTo));
		getActivity().finish();
	}

	protected void gotoClassFromHome(Class<?> classTo) {
		startActivity(new Intent(getActivity().getBaseContext(), classTo));
	}

	protected void gotoClassFromHome(Class<?> classTo, int day) {
		Intent intent = new Intent(getActivity().getBaseContext(), classTo);
		intent.putExtra(Constants.SP_DAY, day);
		startActivity(intent);
	}

	protected void gotoClass(Intent intent) {
		startActivity(intent);
		getActivity().finish();
	}

	protected void goBack() {
		HomeFragment homeFragment = new HomeFragment();
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.base_container, homeFragment);
		ft.commit();
	}

	protected void setUpListHeader(View view, String heading, String sub) {
	}

}
