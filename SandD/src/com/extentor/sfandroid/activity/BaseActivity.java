package com.extentor.sfandroid.activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.extentor.sfandroid.R;
import com.extentor.sfandroid.util.Constants;

public class BaseActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}

	protected void setPageTitle(int resId) {
		((TextView) findViewById(R.id.title)).setText(getString(resId));

	}

	protected void setPageTitle(String title) {
		((TextView) findViewById(R.id.title)).setText(title);

	}

	protected void gotoClass(Class<?> classTo) {
		startActivity(new Intent(this, classTo));
		finish();
	}

	protected void gotoClass(Intent intent) {
		startActivity(intent);
		finish();
	}

	protected boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo()
				.isConnectedOrConnecting());
	}

	protected void showErrorMessage(String msg) {
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(Constants.STR_ERROR);
		alertDialog.setMessage(msg);
		alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,
				Constants.STR_CLOSE, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.dismiss();
					}
				});
		alertDialog.setCancelable(false);
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();
	}

	protected void showErrorMessage(String msg, final Intent intent) {
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(Constants.STR_ERROR);
		alertDialog.setMessage(msg);
		alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,
				Constants.STR_CLOSE, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.dismiss();
						gotoClass(intent);
					}
				});
		alertDialog.setCancelable(false);
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();
	}

	protected void showMessage(String msg) {
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(Constants.STR_INFO);
		alertDialog.setMessage(msg);
		alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,
				Constants.STR_CLOSE, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.dismiss();
					}
				});
		alertDialog.setCancelable(false);
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();
	}

	protected void showMessage(String msg, final Intent intent) {
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(Constants.STR_INFO);
		alertDialog.setMessage(msg);
		alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,
				Constants.STR_CLOSE, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.dismiss();
						gotoClass(intent);
					}
				});
		alertDialog.setCancelable(false);
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();
	}

	protected void replaceFragment(Fragment fragment, int resId) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(resId, fragment);
		ft.commit();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		overridePendingTransition( 0, R.anim.slide_out );
	}
}
