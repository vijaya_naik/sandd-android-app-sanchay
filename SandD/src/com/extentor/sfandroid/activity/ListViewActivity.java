package com.extentor.sfandroid.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.custom.base.Adapter.BaseAdapter;
import com.extentor.custom.base.Helper.RecyclerItemClickListener;
import com.extentor.custom.base.Model.BaseModel;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfobjects.ISFObject;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ListViewActivity extends Activity
{
	private RecyclerView	mList;
	private ImageView		navigationmenu_handle_left;

	private BaseAdapter		bAdapter;
	List<BaseModel>			data			= new ArrayList<BaseModel>();
	private String			soupName		= null;
	ArrayList<JSONObject>	result			= null;
	int						recordCount		= 0;
	private boolean			loading			= true;
	int						pastVisiblesItems, visibleItemCount,
			totalItemCount;
	LinearLayoutManager		mLayoutManager;
	final int				kRecordsToFetch	= 20;
	int						limit			= 0, offset = 0;

	ISFObject				m_Object;
	String					id				= null;
	String					parentSoup		= null;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		this.requestWindowFeature( Window.FEATURE_NO_TITLE );

		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_list_view );

		Bundle extras = getIntent().getExtras();
		m_Object = (ISFObject) extras.getParcelable( "object" );
		soupName = m_Object.getSoupName();
		try
		{
			JSONObject _obj = m_Object.getData();
			id = _obj.getString( "id" );
			parentSoup = _obj.getString( "parent" );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		navigationmenu_handle_left = (ImageView) findViewById( R.id.navigationmenu_handle_left );
		navigationmenu_handle_left.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				finish();
				overridePendingTransition( R.anim.back_in, R.anim.back_out );
			}
		} );

		setupList();
		setUpListData( mList );

		mLayoutManager = new LinearLayoutManager( this );
		mList.setLayoutManager( mLayoutManager );

		result = new ArrayList<JSONObject>();
		DataTask dataTask = new DataTask( soupName, parentSoup, id );
		dataTask.execute();
	}

	public void setupList()
	{
		mList = (RecyclerView) findViewById( R.id.baseList );
		mList.getItemAnimator().setAddDuration( 1000 );
		mList.getItemAnimator().setChangeDuration( 1000 );
		mList.getItemAnimator().setMoveDuration( 1000 );
		mList.getItemAnimator().setRemoveDuration( 1000 );
		mList.setLayoutManager( new LinearLayoutManager( this ) );
		mList.addOnScrollListener( new RecyclerView.OnScrollListener()
		{
			@Override
			public void onScrolled( RecyclerView recyclerView, int dx, int dy )
			{
				visibleItemCount = mLayoutManager.getChildCount();
				totalItemCount = mLayoutManager.getItemCount();
				pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

				if ( loading )
				{
					if ( ( visibleItemCount + pastVisiblesItems ) >= totalItemCount )
					{
						loading = false;
						DataTask dataTask = new DataTask( soupName, parentSoup, id );
						dataTask.execute();
					}
				}
			}
		} );
	}

	private void setUpListData( RecyclerView mList )
	{
		bAdapter = new BaseAdapter( this, data );
		mList.setAdapter( bAdapter );
		mList.addOnItemTouchListener( new RecyclerItemClickListener( this, new RecyclerItemClickListener.OnItemClickListener()
		{
			@Override
			public void onItemClick( View view, int position )
			{
				// TODO Handle item click
				callNavigation( view, position );
			}
		} ) );
	}

	private void callNavigation( View view, int position )
	{
		m_Object.setObject( result.get( position ) );
		Home.doRecordViewNavigation( ListViewActivity.this, m_Object );
	}

	public void addData( JSONObject obj )
	{
		BaseModel navItem = new BaseModel();
		String name = "";
		try
		{
			name = obj.getString( "Name" );
		}
		catch ( JSONException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		navItem.setTitle( name );
		data.add( navItem );
	}

	private class DataTask extends AsyncTask<Void, Void, Void>
	{
		private String	soupName, parentSoup, id;
		JSONArray		_result	= null, _result2 = null;

		public DataTask( String soupName, String parentSoup, String id )
		{
			this.soupName = soupName;
			this.parentSoup = parentSoup;
			this.id = id;
		}

		@Override
		protected Void doInBackground( Void... params )
		{
			// TODO Auto-generated method stub
			SmartStore store = SFApp.GetSmartStore();
			try
			{
				if ( recordCount == 0 )
				{
					_result = store.query( QuerySpec.buildSmartQuerySpec( "select Count(*) from {" + this.soupName + "}", 10 ), 0 );
					recordCount = _result.getJSONArray( 0 ).getInt( 0 );
				}

				if ( ( recordCount - result.size() ) < kRecordsToFetch )
					limit = recordCount - result.size();
				else
					limit = kRecordsToFetch;

				String query = "";
				query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "} where {" + this.soupName + ":" + this.parentSoup + "} = '" + this.id + "' order by {" + this.soupName + ":Name}";
//				if( this.soupName.equals( "Scheme__c" ))
//				{
//					query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "} where {" + this.soupName + ":Id} IN ( select {Scheme_Junction__c:Scheme__c} from {Scheme_Junction__c} where {Scheme_Junction__c:Product__c} = '" + this.id + "')" + " order by {" + this.soupName + ":Name}";
//				}
				if( this.soupName.equals( "Pricebook2" ))
				{
					query = "select {" + this.soupName + ":_soup} from {" + this.soupName + "} where {" + this.soupName + ":Id} IN ( select {PricebookEntry:Pricebook2Id} from {PricebookEntry} where {PricebookEntry:Product2Id} = '" + this.id + "')" + " order by {" + this.soupName + ":Name}";
				}
				_result2 = store.query( QuerySpec.buildSmartQuerySpec( query, limit ), offset++ );
				int size = _result2.length();

				for ( int i = 0; i < size; i++ )
				{
					JSONArray arr = _result2.getJSONArray( i );
					JSONObject obj = arr.getJSONObject( 0 );
					result.add( obj );
					addData( obj );
				}
			}
			catch ( JSONException e )
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute( Void result )
		{
			loading = true;
			bAdapter.notifyDataSetChanged();
		}
	}

	OnClickListener	BackListener	= new OnClickListener()
	{
		@Override
		public void onClick( View v )
		{
			onBackPressed();
		}
	};

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition( R.anim.back_in, R.anim.back_out );
	}
}
