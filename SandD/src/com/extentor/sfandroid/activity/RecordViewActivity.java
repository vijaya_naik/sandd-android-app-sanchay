package com.extentor.sfandroid.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.extentor.custom.base.CustomView.ViewPager.ParallaxFragmentPagerAdapter;
import com.extentor.custom.base.CustomView.ViewPager.ParallaxViewPagerBaseActivity;
import com.extentor.custom.base.CustomView.slidingTab.SlidingTabLayout;
import com.extentor.dialogs.SyncProgressDialog;
import com.extentor.helper.GPSHelper;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.fragments.BaseRecyclerViewFragment;
import com.extentor.sfobjects.ISFObject;

import android.content.Intent;
import android.content.res.Configuration;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RecordViewActivity extends ParallaxViewPagerBaseActivity
{
	private SlidingTabLayout	mNavigBar;
	private LinearLayout		employee_layout;
	@SuppressWarnings("unused")
	private RelativeLayout		mToolbar;

	JSONObject					details;
	JSONObject					fields;
	JSONArray					related;
	private TextView			textTitle, textSubtitle;
	private ImageView			navigationmenu_handle_left;
	LocationListener			m_listener	= null;
	SyncProgressDialog			progress	= null;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		this.requestWindowFeature( Window.FEATURE_NO_TITLE );

		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_record_view );
		initValues();

		textTitle = (TextView) findViewById( R.id.textTitle );
		textSubtitle = (TextView) findViewById( R.id.textSubtitle );

		if ( savedInstanceState != null )
		{
			employee_layout.setTranslationY( savedInstanceState.getFloat( IMAGE_TRANSLATION_Y ) );
			mHeader.setTranslationY( savedInstanceState.getFloat( HEADER_TRANSLATION_Y ) );
		}

		Bundle extras = getIntent().getExtras();
		final ISFObject obj = (ISFObject) extras.getParcelable( "object" );

		try
		{
			textTitle.setText( obj.getDetails().getString( "Name" ) );
			String phone = obj.getDetails().getString( "Phone" );
			if ( !( phone == null || phone.equals( "null" ) ) )
				textSubtitle.setText( phone );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		details = obj.getDetails();
		fields  = obj.getFieldKeyVals();
		related = obj.getRelated();
		
		if( obj.getSoupName().equals( "Account" ) )
		{
			ImageView addOrder = (ImageView) findViewById( R.id.addOrder );
			addOrder.setVisibility( View.VISIBLE );
			addOrder.setOnClickListener( new OnClickListener()
			{
				@Override
				public void onClick( View v )
				{	
//					GPSHelper gpsHelper = GPSHelper.getInstance( RecordViewActivity.this, obj );
//					if( !gpsHelper.checkGPS() )
//					{
//						return;
//					}
//					gpsHelper.getGPS();
					Intent intentAddOrder = new Intent( RecordViewActivity.this, AddOrderActivity.class );
					intentAddOrder.putExtra( "dist", obj.getData().toString() );
					startActivity( intentAddOrder );
					overridePendingTransition( R.anim.navigate_in, R.anim.navigate_out );
				}
			} );
		}

		setupAdapter();
	}

	@Override
	protected void initValues()
	{
		int tabHeight = getResources().getDimensionPixelSize( R.dimen.tab_height );
		mMinHeaderHeight = getResources().getDimensionPixelSize( R.dimen.min_header_height );
		mHeaderHeight = getResources().getDimensionPixelSize( R.dimen.header_height_pager );
		mMinHeaderTranslation = -mMinHeaderHeight + tabHeight;
		mNumFragments = 2;

		employee_layout = (LinearLayout) findViewById( R.id.employee_detail_layout );
		mViewPager = (ViewPager) findViewById( R.id.view_pager );
		mNavigBar = (SlidingTabLayout) findViewById( R.id.navig_tab );
		mHeader = findViewById( R.id.header );

		mToolbar = (RelativeLayout) findViewById( R.id.tool_bar );

		navigationmenu_handle_left = (ImageView) findViewById( R.id.navigationmenu_handle_left );

		navigationmenu_handle_left.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				finish();
				overridePendingTransition( R.anim.back_in, R.anim.back_out );
			}
		} );
		// setSupportActionBar( mToolbar );
		// getSupportActionBar().setHomeButtonEnabled( true );
		// getSupportActionBar().setDisplayHomeAsUpEnabled( true );

	}

	@Override
	protected void onSaveInstanceState( Bundle outState )
	{
		outState.putFloat( IMAGE_TRANSLATION_Y, employee_layout.getTranslationY() );
		outState.putFloat( HEADER_TRANSLATION_Y, mHeader.getTranslationY() );
		super.onSaveInstanceState( outState );
	}

	@Override
	protected void setupAdapter()
	{
		if ( mAdapter == null )
		{
			mAdapter = new ViewPagerAdapter( getSupportFragmentManager(), mNumFragments );
		}

		mViewPager.setAdapter( mAdapter );
		mViewPager.setOffscreenPageLimit( mNumFragments );
		mNavigBar.setOnPageChangeListener( getViewPagerChangeListener() );
		mNavigBar.setViewPager( mViewPager );
	}

	@Override
	protected void scrollHeader( int scrollY )
	{
		float translationY = Math.max( -scrollY, mMinHeaderTranslation );
		mHeader.setTranslationY( translationY );
		employee_layout.setTranslationY( -translationY / 2 );
	}
	
	@Override
	public void onConfigurationChanged( Configuration newConfig )
	{
		super.onConfigurationChanged( newConfig );
	}

	private class ViewPagerAdapter extends ParallaxFragmentPagerAdapter
	{
		public ViewPagerAdapter( FragmentManager fm, int numFragments )
		{
			super( fm, numFragments );
		}

		@Override
		public Fragment getItem( int position )
		{
			Fragment fragment;
			switch ( position )
			{
				case 0 :
					fragment = BaseRecyclerViewFragment.newInstance( 0 );
					break;

				case 1 :
					fragment = BaseRecyclerViewFragment.newInstance( 1 );
					break;

				default :
					throw new IllegalArgumentException( "Wrong page given " + position );
			}

			( (BaseRecyclerViewFragment) fragment ).SetDataSources( details, fields, related );
			return fragment;
		}

		@Override
		public CharSequence getPageTitle( int position )
		{
			switch ( position )
			{
				case 0 :
					return "DETAILS";

				case 1 :
					return "RELATED";

				default :
					throw new IllegalArgumentException( "wrong position for the fragment in vehicle page" );
			}
		}
	}
	
	OnClickListener BackListener = new OnClickListener()
	{
		@Override
		public void onClick( View v )
		{
			onBackPressed();
		}
	};

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition( R.anim.back_in, R.anim.back_out );
	}
}
