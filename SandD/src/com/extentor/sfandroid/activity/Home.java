package com.extentor.sfandroid.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.extentor.dialogs.SyncProgressDialog;
import com.extentor.helper.GPSHelper;
import com.extentor.helper.SoupHelper;
import com.extentor.interfaces.IDataFetch;
import com.extentor.interfaces.IFragmentDrawerListener;
import com.extentor.interfaces.IStrategy;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfandroid.fragments.FragmentDrawer;
import com.extentor.sfandroid.fragments.HomeFragment;
import com.extentor.sfandroid.fragments.RecyclerFragment;
import com.extentor.sfobjects.Account;
import com.extentor.sfobjects.ISFObject;
import com.extentor.sfobjects.Order;
import com.extentor.sfobjects.Product;
import com.extentor.sfobjects.Scheme;
import com.extentor.sfobjects.Visit;
import com.extentor.task.Executor;
import com.extentor.task.FetchAllSoups;
import com.extentor.task.FetchObjectDescription;
import com.extentor.task.FetchUserDetails;
import com.extentor.task.PushNewObject;
import com.extentor.task.SyncDeletedRecords;
import com.extentor.task.TaskQueue;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestClient.ClientInfo;
import com.salesforce.androidsdk.smartstore.ui.SmartStoreInspectorActivity;
import com.salesforce.androidsdk.smartsync.app.SmartSyncSDKManager;
import com.salesforce.androidsdk.smartsync.manager.SyncManager;
import com.salesforce.androidsdk.smartsync.manager.SyncManager.SmartSyncException;
import com.salesforce.androidsdk.smartsync.manager.SyncManager.SyncUpdateCallback;
import com.salesforce.androidsdk.smartsync.util.SOQLBuilder;
import com.salesforce.androidsdk.smartsync.util.SoqlSyncDownTarget;
import com.salesforce.androidsdk.smartsync.util.SyncDownTarget;
import com.salesforce.androidsdk.smartsync.util.SyncOptions;
import com.salesforce.androidsdk.smartsync.util.SyncState.MergeMode;
import com.salesforce.androidsdk.smartsync.util.SyncState.Status;
import com.salesforce.androidsdk.ui.sfnative.SalesforceActivity;
import com.extentor.helper.Helper;

public class Home extends SalesforceActivity implements IDataFetch, IFragmentDrawerListener, View.OnClickListener
{
	ImageView				addOrder;
	ImageView				navigationdrawerHandleLeft;
	ListView				notificationDrawer;
	DrawerLayout			lDrawer;
	ListView				stageLeftDrawer;
	Vector<Fragment>		fragments			= new Vector<Fragment>();
	private FragmentDrawer	drawerFragment;
	private List<String>	m_Soups;
	private long			syncId				= -1;
	private Integer			limit				= 0;
	private Integer			recordsFetched		= 0;
	private String			soupName			= null;
	private TextView		title;
	private RelativeLayout	headerSearch;
	private EditText		editTextSearch;
	private Button			editTextClear;
	Fragment				fragment			= null;
	SyncProgressDialog		progress			= null;
	RestClient				m_client;
	Boolean					initialSyncRunning	= false;
	Boolean					initialSyncDone		= false;
	Long					systemTimeMillis	= 0L;
	Boolean					isOrderPage			= false;
	LocationListener		m_listener			= null;
	boolean					m_GPS_Enabled		= true;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		this.requestWindowFeature( Window.FEATURE_NO_TITLE );

		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );
		
		initialSyncDone = Helper.GetSharedPrefItemBoolean( "initialSyncDone" );
		
		headerSearch = (RelativeLayout) findViewById( R.id.headerSearch );
		editTextSearch = (EditText) findViewById( R.id.editTextSearch );
		editTextSearch.setImeOptions( EditorInfo.IME_ACTION_DONE );
		editTextSearch.addTextChangedListener( new TextWatcher()
		{
			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count )
			{
				// get text, fire text to search
				if ( !( fragment instanceof HomeFragment ) && !s.equals( "" ) )
				{
					( (RecyclerFragment) fragment ).PerformSearch( s.toString() );
				}
			}

			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after )
			{
			}

			@Override
			public void afterTextChanged( Editable s )
			{
			}
		} );
		editTextClear = (Button) findViewById( R.id.editTextClear );
		editTextClear.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				editTextSearch.setText( "" );

				InputMethodManager inputMethodManager = (InputMethodManager) getSystemService( Activity.INPUT_METHOD_SERVICE );
				inputMethodManager.hideSoftInputFromWindow( getCurrentFocus().getWindowToken(), 0 );

				( (RecyclerFragment) fragment ).PerformSearch( null );
			}
		} );
		
		addOrder = (ImageView) findViewById( R.id.addOrder );
		addOrder.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
//				GPSHelper gpsHelper = GPSHelper.getInstance( Home.this );
//				if( !gpsHelper.checkGPS() )
//				{
//					return;
//				}
//				gpsHelper.getGPS();
				
				Intent intentAddOrder = new Intent( Home.this, AddOrderActivity.class );
				startActivity( intentAddOrder );
				overridePendingTransition( R.anim.navigate_in, R.anim.navigate_out );
			}
		} );

		title = (TextView) findViewById( R.id.title );
		setupDrawer();

		Button buttonInspect = (Button) findViewById( R.id.buttonInspect );
		buttonInspect.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				final Intent i = new Intent( Home.this, SmartStoreInspectorActivity.class );
				startActivity( i );
			}
		} );
		
		ImageView options = (ImageView) findViewById( R.id.options );
		options.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				PopupMenu popup = new PopupMenu( Home.this, v );
				popup.inflate( R.menu.main );
				if( isOrderPage )
				{
					popup.getMenu().getItem( 1 ).setVisible( true );
				}
				popup.setOnMenuItemClickListener( new OnMenuItemClickListener()
				{
					@Override
					public boolean onMenuItemClick( MenuItem arg0 )
					{
						Home.this.onMenuItemSelected( arg0 );
						return true;
					}
				} );
				popup.show();
			}
		} );
	}

	@Override
	public void onResume()
	{
		super.onResume();
		GPSHelper gpsHelper = GPSHelper.getInstance( Home.this );
		gpsHelper.checkGPS();
	}

	static int i = 0;
	@Override
	public void onResume( RestClient client )
	{
		Log.d( "ON RESUME CALLED", Integer.toString( i++ ) + " times");
		m_client = client;
		if ( Helper.isInternetAvailable( getApplicationContext(), Home.this, false ) )
		{
			ClientInfo info = client.getClientInfo();
			if ( info != null )
			{
				String email = Helper.GetSharedPrefItem( "email" );
				if( email != null && !email.equals( info.username ) )
				{
					//delete previously saved orders if new user logs in
					Helper.EditSharedPrefs( getString( R.string.NEW_ORDER ), "" );
				}
				
				Helper.EditSharedPrefs( "email", info.username );
				Helper.EditSharedPrefs( "userid", info.userId );
				Helper.EditSharedPrefs( "instanceurl", info.instanceUrl.toString() );
			}
		}
		
		SFApp.m_SmartSyncManager = SmartSyncSDKManager.getInstance();
		SFApp.m_SmartStore = SFApp.m_SmartSyncManager.getSmartStore();
		
		UpdateFragments();
		
		if ( !Helper.GetSharedPrefItemBoolean( "initialSyncDone" ) && !initialSyncRunning )
		{
			initialSyncRunning = true;
			fireSync();
		}
	}

	@Override
	public void onConfigurationChanged( Configuration newConfig )
	{
		super.onConfigurationChanged( newConfig );
		if ( newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE )
		{
		}
		else if ( newConfig.orientation == Configuration.ORIENTATION_PORTRAIT )
		{
		}
	}
	
	private void onMenuItemSelected( MenuItem item )
	{
		if( item.getOrder() == 100 )
		{
			//sync
			displayView( 0 );
			fireSync();
		}
		else
		{
			//view orders
			Intent intent = new Intent( Home.this, SavedOrdersActivity.class );
			startActivity( intent );
			overridePendingTransition( R.anim.navigate_in, R.anim.navigate_out );
		}
	}
	
	private void logout()
	{
		String m_Orders = Helper.GetSharedPrefItem( getString( R.string.NEW_ORDER ) );
		String m_Email = Helper.GetSharedPrefItem( "email" );
		Helper.ClearSharedPrefs();

		lDrawer.closeDrawer( Gravity.START );
		SalesforceSDKManager.getInstance().logout( Home.this );
		
		Helper.EditSharedPrefs( getString( R.string.NEW_ORDER ), m_Orders );
		Helper.EditSharedPrefs( "email", m_Email );
	}

	public void performLogout()
	{
		String m_Order = Helper.GetSharedPrefItem( getString( R.string.NEW_ORDER ) );
		if( m_Order != null && !m_Order.isEmpty() )
		{
			try
			{
				JSONArray arr = new JSONArray( m_Order );
				int count = arr.length();
				if( count > 0 )
				{
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this );
					alertDialogBuilder.setMessage( "Unsynced Order Data present. Are you sure you want to logout?" ).setCancelable( false ).setPositiveButton( "Yes", new DialogInterface.OnClickListener()
					{
						public void onClick( DialogInterface dialog, int id )
						{
							logout();
						}
					} );
					alertDialogBuilder.setNegativeButton( "No", new DialogInterface.OnClickListener()
					{
						public void onClick( DialogInterface dialog, int id )
						{
							dialog.cancel();
						}
					} );
					AlertDialog alert = alertDialogBuilder.create();
					alert.show();
				}
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
		}
		else
		{
			logout();
		}
	}

	private void setupDrawer()
	{
		lDrawer = (DrawerLayout) findViewById( R.id.DrawerLayout );
		navigationdrawerHandleLeft = (ImageView) findViewById( R.id.navigationmenu_handle_left );
		navigationdrawerHandleLeft.setOnClickListener( this );
		drawerFragment = new FragmentDrawer();
		drawerFragment.setUp( R.id.fragment_navigation_drawer, lDrawer );
		drawerFragment.setDrawerListener( this );
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace( R.id.fragment_navigation_drawer, drawerFragment );
		ft.commit();

		displayView( 0 );
	}

	public void UpdateFragments()
	{
		FragmentManager fm = getFragmentManager();
		Fragment fragment = null;
//		fragment = (HomeFragment) fm.findFragmentById( R.id.base_container );
//		((HomeFragment)fragment).Update();
		fragment = (FragmentDrawer) fm.findFragmentById( R.id.fragment_navigation_drawer );
		((FragmentDrawer)fragment).Update();
	}
	
	public void UpdateFragmentErrors()
	{
//		FragmentManager fm = getFragmentManager();
//		Fragment fragment = null;
//		fragment = (HomeFragment) fm.findFragmentById( R.id.base_container );
//		((HomeFragment)fragment).UpdateErrors();
	}

	@Override
	public void onClick( View v )
	{
		switch ( v.getId() )
		{
			case R.id.navigationmenu_handle_left :
				lDrawer.openDrawer( Gravity.START );
				break;
	
			default :
				break;
		}
	}

	@Override
	public void onDrawerItemSelected( View view, int position )
	{
		displayView( position );
		lDrawer.closeDrawer( Gravity.START );
	}

	public void fireSync()
	{
		if ( !Helper.isInternetAvailable( getApplicationContext(), Home.this, true ) )
		{
			return;
		}
		
		SyncProgressDialog.showProgressBar( Home.this, false );
		progress = SyncProgressDialog.getProgressDialog();
		
		final TaskQueue queue = TaskQueue.getInstance();
		queue.Clear();
		String query = "SELECT Id, Name FROM User WHERE Id='" + Helper.GetSharedPrefItem( "userid" ) + "'";
		queue.AddTask( new Executor( new FetchUserDetails( m_client, getString(R.string.api_version), query, this ) ) );
		queue.AddTask( new Executor( new PushNewObject(this, this ) ) );
		queue.AddTask( new Executor( new FetchAllSoups(m_client, getString( R.string.getAllSoup_EndPoint ), this ) ) );
		queue.AddTask( new Executor( new FetchObjectDescription(m_client, getString( R.string.describeSObject_EndPoint ), this ) ) );
		queue.AddTask( new Executor( new IStrategy()
		{
			@Override
			public void doOperation()
			{
				UpdateFragments();
				queue.RunNext();
			}
		} ) );
		queue.AddTask( new Executor( new IStrategy()
		{
			@Override
			public void doOperation()
			{
				m_Soups = SoupHelper.GetAllSoupNames();
				m_Soups.remove( "syncs_soup" );
				
				if( m_Soups.size() == 0 )
				{
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);
					alertDialog.setTitle("Alert!");
					alertDialog.setMessage("Application error! Please re-login.");
					Dialog alert = alertDialog.create();
					alert.show();
					return;
				}
				dataSyncHandler.sendEmptyMessage( 0 );
			}
		} ) );
		queue.AddTask( new Executor( new SyncDeletedRecords(m_client, getString( R.string.deleteRecords_EndPoint ), this ) ) );
		queue.AddTask( new Executor( new IStrategy()
		{
			@Override
			public void doOperation()
			{
				UpdateFragmentErrors();
			}
		} ) );
		queue.RunNext();
	}

	@Override
	public void onDataFetchStart()
	{
	}

	@Override
	public void onDataFetchSuccess()
	{
		TaskQueue queue = TaskQueue.getInstance();
		queue.RunNext();
	}
	
	@Override
	public void onDataFetchError( Exception exception )
	{
		SyncProgressDialog.hideProgressBar();
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);
		alertDialog.setTitle("Alert!");
		alertDialog.setMessage("Sync error! Please try again.");
		Dialog alert = alertDialog.create();
		alert.show();
	}
	
	public void UpdateProgress( String status, String status2 )
	{
		JSONObject obj = new JSONObject();
		try
		{
			obj.put( "status", status );
			obj.put( "status2", status2 );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		Message msg = new Message();
		msg.obj = obj;
		uiSyncHandler.sendMessage( msg );
	}

	private Handler uiSyncHandler = new Handler()
	{
		@Override
		public void handleMessage( Message msg )
		{
			JSONObject obj = (JSONObject) msg.obj;
			String s1 = "", s2 = "";
			try
			{
				s1 = obj.getString( "status" );
				s2 = obj.getString( "status2" );
			}
			catch ( JSONException e )
			{
				e.printStackTrace();
			}
			progress.SetStatusMessage( s1, s2 );
		}
	};

	private Handler	dataSyncHandler	= new Handler()
	{
		@Override
		public void handleMessage( Message msg )
		{
			if ( msg.what == -1 )
			{
				SyncProgressDialog.hideProgressBar();
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);
				alertDialog.setTitle("Alert!");
				alertDialog.setMessage("Sync error! Please try again.");
				Dialog alert = alertDialog.create();
				alert.show();
				return;
			}

			if ( m_Soups.size() != 0 )
			{
				soupName = m_Soups.remove( 0 );
			}
			else
			{
				initialSyncRunning = false;
				SyncProgressDialog.hideProgressBar();
				initialSyncDone = true;
				Helper.EditSharedPrefs( "initialSyncDone", initialSyncDone );
				final TaskQueue queue = TaskQueue.getInstance();
				queue.RunNext();
				return;
			}
			dataSyncDown();
		}
	};
	
	private void dataSyncDown()
	{
		SyncManager syncManager = SyncManager.getInstance();

		final String soup = soupName;
		final SyncUpdateCallback callback = new SyncUpdateCallback()
		{
			@Override
			public void onUpdate( com.salesforce.androidsdk.smartsync.util.SyncState sync )
			{
				if ( Looper.myLooper() == null )
				{
					Looper.prepare();
				}
				if ( Status.DONE.equals( sync.getStatus() ) )
				{
					Log.i( "SYNC DOWN", "Sync success for: " + soup );
					
					JSONObject obj = new JSONObject();
					try
					{
						obj.put( "status", "Synced " + Helper.ProcessKey( soup ) );
						obj.put( "status2", "");
					}
					catch ( JSONException e )
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Message msg = new Message();
					msg.obj = obj;
					uiSyncHandler.sendMessage( msg );
					
					recordsFetched += limit;
					
					if( sync.getTotalSize() != 0 )
					{
						Helper.EditSharedPrefs( soupName+"_time", systemTimeMillis );
					}
					dataSyncHandler.sendEmptyMessage( 0 );
				}
				if ( Status.RUNNING.equals( sync.getStatus() ) )
				{
					Log.i( "SYNC DOWN - RUNNING", soup );
					
					JSONObject obj = new JSONObject();
					try
					{
						obj.put( "status", "Sync running for " + Helper.ProcessKey( soup ) );
						obj.put( "status2", sync.getProgress() + "%" );
					}
					catch ( JSONException e )
					{
						e.printStackTrace();
					}
					Message msg = new Message();
					msg.obj = obj;
					uiSyncHandler.sendMessage( msg );
				}
				if ( Status.FAILED.equals( sync.getStatus() ) )
				{
					Log.i( "SYNC DOWN - FAILED", Helper.ProcessKey( soup ) );
					JSONObject obj = new JSONObject();
					try
					{
						obj.put( "status", "Sync failure for " + soup );
						obj.put( "status2", "" );
					}
					catch ( JSONException e )
					{
						e.printStackTrace();
					}
					Message msg = new Message();
					msg.obj = obj;
					uiSyncHandler.sendMessage( msg );
					dataSyncHandler.sendEmptyMessage( -1 );
				}
			}
		};
		
		ArrayList<String> fields = SoupHelper.GetSoupFieldNamesList( soupName );
		final SyncOptions option = SyncOptions.optionsForSyncDown( MergeMode.OVERWRITE );
		String soqlQuery = SOQLBuilder.getInstanceWithFields( fields ).from( soupName ).limit( 10000 ).build();
		Long timestamp = Helper.GetSharedPrefItemLong( soupName+"_time" );
		if( timestamp != -1 )
		{
			soqlQuery = SoqlSyncDownTarget.addFilterForReSync( soqlQuery, timestamp );
		}
		final SyncDownTarget target = new SoqlSyncDownTarget( soqlQuery );
		try
		{
			syncId = Helper.GetSharedPrefItemLong( soupName );
//			if ( syncId == -1 )
//			{
			systemTimeMillis = System.currentTimeMillis();
			final com.salesforce.androidsdk.smartsync.util.SyncState sync = syncManager.syncDown( target, option, soupName, callback );
			syncId = sync.getId();
			Helper.EditSharedPrefs( soupName, syncId );
//			}
//			else
//			{
//				syncManager.reSync( syncId, callback );
//			}
		}
		catch ( JSONException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e( "SYNC DOWN", "JSONException occurred while parsing", e );
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);
			alertDialog.setTitle("Alert!");
			alertDialog.setMessage("Sync down error! Please try again.");
			Dialog alert = alertDialog.create();
			alert.show();
		}
		catch ( SmartSyncException e )
		{
			e.printStackTrace();
			Log.e( "SYNC DOWN", "SmartSyncException occurred while attempting to sync down", e );
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(Home.this);
			alertDialog.setTitle("Alert!");
			alertDialog.setMessage("Sync down error! Please try again.");
			Dialog alert = alertDialog.create();
			alert.show();
		}
	}

	private void displayView( int position )
	{
		isOrderPage = false;
		headerSearch.setVisibility( View.VISIBLE );
		addOrder.setVisibility( View.GONE );
		switch ( position )
		{
			case 0 :
				headerSearch.setVisibility( View.GONE );
				fragment = new HomeFragment();
				title.setText( getString( R.string.title_home ) );
				break;

			case 1 :
				editTextSearch.setHint( "Search Accounts" );
				editTextSearch.setText( "" );
				fragment = new RecyclerFragment( new Account() );
				title.setText( getString( R.string.nav_item_Accounts ) );
				break;

			case 2 :
				isOrderPage = true;
				addOrder.setVisibility( View.VISIBLE );
				editTextSearch.setHint( "Search Orders" );
				editTextSearch.setText( "" );
				fragment = new RecyclerFragment( new Order() );
				title.setText( getString( R.string.nav_item_Orders ) );
				break;

			case 3 :
				editTextSearch.setHint( "Search Visits" );
				editTextSearch.setText( "" );
				fragment = new RecyclerFragment( new Visit() );
				title.setText( getString( R.string.nav_item_Visits ) );
				break;

			case 4 :
				editTextSearch.setHint( "Search Products" );
				editTextSearch.setText( "" );
				fragment = new RecyclerFragment( new Product() );
				title.setText( getString( R.string.nav_item_Products ) );
				break;
				
			case 5 :
				editTextSearch.setHint( "Search Schemes" );
				editTextSearch.setText( "" );
				fragment = new RecyclerFragment( new Scheme() );
				title.setText( getString( R.string.nav_item_Schemes ) );
				break;

			default :
				break;
		}

		if ( fragment != null )
		{
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace( R.id.base_container, fragment );
			fragmentTransaction.commit();
		}
	}

	public static void doRecordViewNavigation( Activity context, ISFObject obj )
	{
		Intent intent = new Intent( context, RecordViewActivity.class );
		intent.putExtra( "object", obj );
		context.startActivity( intent );
		context.overridePendingTransition( R.anim.navigate_in, R.anim.navigate_out );
	}

	public static void doListViewNavigation( Activity context, View view, ISFObject obj )
	{
		Intent intent = new Intent( context, ListViewActivity.class );
		intent.putExtra( "object", obj );
		context.startActivity( intent );
		context.overridePendingTransition( R.anim.navigate_in, R.anim.navigate_out );
	}
}
