package com.extentor.sfandroid.activity;

import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.sfandroid.R;
import com.extentor.sfandroid.fragments.FragmentOrder;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class AddOrderActivity extends Activity
{
	Fragment	fragment	= null;
	JSONObject	order		= null;
	String		distributor	= null;
	Location	location	= null;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_add_order );
		
		Bundle extras = getIntent().getExtras();
		if( extras != null )
		{
			distributor = extras.getString( "dist" );
			location = (Location) extras.get( getString( R.string.ORDER_LOCATION ) );
		}
		
		displayView();
		order = new JSONObject();
	}
	
	

	private void displayView()
	{
		if( fragment == null )
			fragment = new FragmentOrder();
		if( location != null )
		{
			((FragmentOrder)fragment).location = location;
		}
		if( distributor != null )
		{
			Bundle bundle = new Bundle();
			bundle.putString( "dist", distributor );
			fragment.setArguments( bundle );
		}
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace( R.id.base_container, fragment );
		fragmentTransaction.commit();
	}
	
	OnClickListener	BackListener	= new OnClickListener()
	{
		@Override
		public void onClick( View v )
		{
			onBackPressed();
		}
	};

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition( R.anim.back_in, R.anim.back_out );
	}
	
	public void UpdateDistributor( String key, JSONObject object )
	{
		try
		{
			order.put( key, object );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		((FragmentOrder)fragment).UpdateDistributor( object );
	}
	
	public void UpdateOrderItem( JSONObject displayData, JSONObject orderData )
	{
		((FragmentOrder)fragment).UpdateOrderItem( displayData, orderData );
	}	
}
