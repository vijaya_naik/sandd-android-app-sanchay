package com.extentor.sfandroid.activity;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.extentor.custom.base.Adapter.BaseAdapter;
import com.extentor.custom.base.Adapter.BaseAdapter1;
import com.extentor.custom.base.Helper.RecyclerItemClickListener;
import com.extentor.custom.base.Model.BaseModel;
import com.extentor.helper.Helper;
import com.extentor.sfandroid.R;
import com.extentor.sfandroid.SFApp;
import com.extentor.sfobjects.ISFObject;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SavedOrdersActivity extends Activity
{
	private RecyclerView	mList;
	private ImageView		navigationmenu_handle_left;

	private BaseAdapter		bAdapter;
	List<BaseModel>			data			= new ArrayList<BaseModel>();
	ArrayList<JSONObject>	result			= null;
	int						recordCount		= 0;
	private boolean			loading			= true;
	int						pastVisiblesItems, visibleItemCount,
			totalItemCount;
	LinearLayoutManager		mLayoutManager;
	final int				kRecordsToFetch	= 20;
	int						limit			= 0, offset = 0;

	ISFObject				m_Object;
	String					id				= null;
	String					parentSoup		= null;
	ArrayList<JSONObject>	m_savedOrders	= null;
	private TextView		title;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		this.requestWindowFeature( Window.FEATURE_NO_TITLE );

		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_list_view );
		
		title = (TextView) findViewById( R.id.title );
		title.setText( "Saved Orders" );

		navigationmenu_handle_left = (ImageView) findViewById( R.id.navigationmenu_handle_left );
		navigationmenu_handle_left.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				finish();
				overridePendingTransition( R.anim.back_in, R.anim.back_out );
			}
		} );

		setupList();
		setUpListData( mList );

		mLayoutManager = new LinearLayoutManager( this );
		mList.setLayoutManager( mLayoutManager );
		
		getSavedOrders();
	}
	
	private void getSavedOrders()
	{
		JSONArray m_orders = null;
		if( m_savedOrders == null )
		{
			m_savedOrders = new ArrayList<>();
		}
		try
		{
			String _orders = Helper.GetSharedPrefItem( getString( R.string.NEW_ORDER ) );
			if( _orders == null ) return;
			m_orders = new JSONArray( _orders );
			int count = m_orders.length();
			
			for( int i = 0; i < count; i++ )
			{
				m_savedOrders.add( m_orders.getJSONObject( i ) );
				addData( new JSONObject().put( "Name", String.format( "Saved Order # %d", i ) ) );
			}
			bAdapter.notifyDataSetChanged();
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void setupList()
	{
		mList = (RecyclerView) findViewById( R.id.baseList );
		mList.getItemAnimator().setAddDuration( 1000 );
		mList.getItemAnimator().setChangeDuration( 1000 );
		mList.getItemAnimator().setMoveDuration( 1000 );
		mList.getItemAnimator().setRemoveDuration( 1000 );
		mList.setLayoutManager( new LinearLayoutManager( this ) );
		mList.addOnScrollListener( new RecyclerView.OnScrollListener()
		{
			@Override
			public void onScrolled( RecyclerView recyclerView, int dx, int dy )
			{
				visibleItemCount = mLayoutManager.getChildCount();
				totalItemCount = mLayoutManager.getItemCount();
				pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

				if ( loading )
				{
					if ( ( visibleItemCount + pastVisiblesItems ) >= totalItemCount )
					{
						loading = false;
					}
				}
			}
		} );
	}

	private void setUpListData( RecyclerView mList )
	{
		bAdapter = new BaseAdapter( this, data );
		mList.setAdapter( bAdapter );
		mList.addOnItemTouchListener( new RecyclerItemClickListener( this, new RecyclerItemClickListener.OnItemClickListener()
		{
			@Override
			public void onItemClick( View view, final int position )
			{
				AlertDialog.Builder builder = new AlertDialog.Builder( SavedOrdersActivity.this );
				builder.setTitle( "Select Action" ).setItems( new String[]{"View", "Delete"}, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick( DialogInterface dialog, int which )
					{
						switch ( which )
						{
							case 0 :
								// view
								viewOrderItem( position );
								break;
							case 1 :
								// delete
								deleteOrderItem( position );
								break;
							default :
								break;
						}
					}
				} );
				builder.create().show();
			}
		} ) );
	}
	
	private void viewOrderItem( int position )
	{	
		ArrayList<String> keys = new ArrayList<>();
		keys.add( getString( R.string.ORDER_ITEM_PRODUCT_CODE ) );
		keys.add( getString( R.string.ORDER_ITEM_PRODUCT_NAME ) );
		keys.add( getString( R.string.ORDER_ITEM_PRICE_BOOK ) );
		keys.add( getString( R.string.ORDER_ITEM_PRICE ) );
		keys.add( getString( R.string.ORDER_ITEM_QUANTITY ) );
		keys.add( getString( R.string.ORDER_ITEM_UOM ) );
		keys.add( getString( R.string.ORDER_ITEM_DISCOUNT ) );
		keys.add( getString( R.string.ORDER_ITEM_SALES_PRICE ) );
		keys.add( getString( R.string.ORDER_ITEM_NET_PRICE ) );
		
		final Dialog dialog = new Dialog( this, R.style.Theme_AppCompat_Light_Dialog_Alert );
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		View view = this.getLayoutInflater().inflate(R.layout.recycler_listview, null);
		RecyclerView mList = (RecyclerView) view.findViewById( R.id.baseList );
		mList.getItemAnimator().setAddDuration( 1000 );
		mList.getItemAnimator().setChangeDuration( 1000 );
		mList.getItemAnimator().setMoveDuration( 1000 );
		mList.getItemAnimator().setRemoveDuration( 1000 );
		mList.setLayoutManager( new LinearLayoutManager( this ) );
		List<BaseModel> data = new ArrayList<BaseModel>();
		
		JSONObject obj = m_savedOrders.get( position );
		JSONArray orderLineItemsDisplayData = null;
		try
		{
			JSONObject distributor = obj.getJSONObject( "orderRecord" );
			String distId = distributor.getString( "Distributor__c" );
			
			SmartStore store = SFApp.GetSmartStore();
			String query = "select {Account:Name} from {Account} where {Account:Id}='" + distId + "'";
			JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( query, 1 ), 0 );
			
			String distName = result.getJSONArray( 0 ).getString( 0 );
			
			BaseModel navItem = new BaseModel();
			navItem.setTitle( distName );
			data.add( navItem );
			
			String strOrderLines = obj.getString( getString( R.string.ORDER_DISPLAY_DATA ) );
			orderLineItemsDisplayData = new JSONArray( strOrderLines );
			
			JSONObject item = null;
			int count = orderLineItemsDisplayData.length();
			
			for( int i = 0; i < count; i++ )
			{
				item = orderLineItemsDisplayData.getJSONObject( i );
				StringBuilder stringBuilder = new StringBuilder();
				navItem = new BaseModel();
				for( String key : keys )
				{
					if( !item.isNull( key ) )
					{
						try
						{
							String val = item.getString( key );
							stringBuilder.append( String.format( "%s : %s\n", key, val ) );
						}
						catch ( JSONException e )
						{
							e.printStackTrace();
						}
					}
				}
				navItem.setTitle( stringBuilder.toString() );
				data.add( navItem );
			}
		}
		catch ( JSONException e1 )
		{
			e1.printStackTrace();
			return;
		}
		
		mList.setAdapter( new BaseAdapter1( this, data ) );
		dialog.setContentView( view );
		dialog.show();
	}

	private void deleteOrderItem( int position )
	{
		data.remove( position );
		bAdapter.notifyDataSetChanged();
		m_savedOrders.remove( position );
		Helper.EditSharedPrefs( getString( R.string.NEW_ORDER ), m_savedOrders.toString() );
	}

	public void addData( JSONObject obj )
	{
		BaseModel navItem = new BaseModel();
		String name = "";
		try
		{
			name = obj.getString( "Name" );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}

		navItem.setTitle( name );
		data.add( navItem );
	}

	OnClickListener	BackListener	= new OnClickListener()
	{
		@Override
		public void onClick( View v )
		{
			onBackPressed();
		}
	};

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition( R.anim.back_in, R.anim.back_out );
	}
}
