package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.helper.Helper;
import com.extentor.sfandroid.SFApp;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderLineItems extends SFObject
{
	public OrderLineItems()
	{
		super();
		
		m_Display = true;// true for display - false for hide
		m_Type = "Sales_Order_Line_Item__c";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}
	
	public OrderLineItems( Parcel in )
	{
		super( in );
		try
		{
			m_Object.put( "parent", "Sales_Order__c" );
		}
		catch ( JSONException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static final Parcelable.Creator<OrderLineItems> CREATOR = new Parcelable.Creator<OrderLineItems>()
	{
		@Override
		public OrderLineItems createFromParcel( Parcel source )
		{
			return new OrderLineItems( source );
		}

		@Override
		public OrderLineItems[] newArray( int size )
		{
			return new OrderLineItems[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
		checker = new ICheckKeyValue()
		{
			@Override
			public String checkKey( String key )
			{
				String retval = null;
				try
				{
					if ( key.equals( "Price_Book__c" ) )
					{
						String id = m_Object.getString( key );
						SmartStore store = SFApp.GetSmartStore();
						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {Pricebook2:Name} from {Pricebook2} where {Pricebook2:Id}='" + id + "'", 10 ), 0 );
						retval = result.getJSONArray( 0 ).getString( 0 );
					}
					if ( key.equals( "Part__c" ) )
					{
						String id = m_Object.getString( key );
						SmartStore store = SFApp.GetSmartStore();
						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {Product2:Name} from {Product2} where {Product2:Id}='" + id + "'", 10 ), 0 );
						retval = result.getJSONArray( 0 ).getString( 0 );
					}
					if ( key.equals( "Date__c" ) )
					{
						String val = m_Object.getString( key );
						retval = Helper.GetDate( val );
					}
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}

				return retval;
			}
		};
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		return arr;
	}
}
