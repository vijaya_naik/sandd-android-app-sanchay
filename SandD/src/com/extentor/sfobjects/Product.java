package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Product extends SFObject
{
	public Product()
	{
		super();
		
		m_Display = true;// true for display - false for hide
		m_Type = "Product2";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}
	
	public Product( Parcel in )
	{
		super( in );
		try
		{
			m_Object.put( "parent", "Product2" );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}
	
	public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>()
	{
		@Override
		public Product createFromParcel( Parcel source )
		{
			return new Product( source );
		}

		@Override
		public Product[] newArray( int size )
		{
			return new Product[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		try
		{
			JSONObject obj = new JSONObject();
			obj.put( "name", "Price Books" );
			obj.put( "id", m_Object.getString( "Id" ) );
			obj.put( "object", new PriceLists() );
			arr.put( obj );
		}
		catch ( JSONException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;
	}
}
