package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.sfandroid.SFApp;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;

import android.os.Parcel;
import android.os.Parcelable;

public class Visit extends SFObject
{
	public Visit()
	{
		super();
		
		m_Display = true;// true for display - false for hide
		m_Type = "Visit__c";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}
	
	public Visit( Parcel in )
	{
		super( in );
	}
	
	public static final Parcelable.Creator<Visit> CREATOR = new Parcelable.Creator<Visit>()
	{
		@Override
		public Visit createFromParcel( Parcel source )
		{
			return new Visit( source );
		}

		@Override
		public Visit[] newArray( int size )
		{
			return new Visit[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
		checker = new ICheckKeyValue()
		{
			@Override
			public String checkKey( String key )
			{
				String retval = null;
				try
				{
					if ( key.equals( "Account__c" ) )
					{
						String id = m_Object.getString( key );
						SmartStore store = SFApp.GetSmartStore();
						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {Account:Name} from {Account} where {Account:Id}='" + id + "'", 10 ), 0 );
						retval = result.getJSONArray( 0 ).getString( 0 );
					}
					if ( key.equals( "Visit_Plan__c" ) )
					{
						String id = m_Object.getString( key );
						SmartStore store = SFApp.GetSmartStore();
						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {Visit_Plan__c:Name} from {Visit_Plan__c} where {Visit_Plan__c:Id}='" + id + "'", 10 ), 0 );
						retval = result.getJSONArray( 0 ).getString( 0 );
					}
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}
				return retval;
			}
		};
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		return arr;
	}
}
