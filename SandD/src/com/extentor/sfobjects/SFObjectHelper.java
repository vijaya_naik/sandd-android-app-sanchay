package com.extentor.sfobjects;

import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.helper.Helper;

public class SFObjectHelper
{
	public static JSONObject GetFields( String type, String subtype )
	{
		JSONObject fieldOptions = null;
		JSONObject retval = null;
		try
		{
			fieldOptions = new JSONObject( Helper.GetSharedPrefItem( "objectlayouts" ) );
			JSONObject account = fieldOptions.getJSONObject( type );
			
			String key = "Master";
			if( subtype != null )
			{
				Iterator<String> keys = account.keys();
				while ( keys.hasNext() )
				{
					String k = keys.next();
					if ( k.equals( subtype ) )
					{
						key = k;
						break;
					}
				}
			}

			retval = account.getJSONObject( key );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		return retval;
	}
	
	public static ArrayList<String> GetFieldOptions(String type, String subtype)
	{
		ArrayList<String> retval = new ArrayList<String>();
		JSONObject obj = SFObjectHelper.GetFields( type, subtype );
		if( obj == null )
		{
			return retval;
		}
		Iterator<String> keys = obj.keys();
		while( keys.hasNext() )
		{
			retval.add( keys.next() );
		}
		return retval;
	}
}
