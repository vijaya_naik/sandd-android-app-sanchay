package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class Scheme extends SFObject
{
	public Scheme()
	{
		super();

		m_Display = true;// true for display - false for hide
		m_Type = "Scheme__c";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}

	public Scheme( Parcel in )
	{
		super( in );
	}

	public static final Parcelable.Creator<Scheme>	CREATOR	= new Parcelable.Creator<Scheme>()
	{
		@Override
		public Scheme createFromParcel( Parcel source )
		{
			return new Scheme( source );
		}

		@Override
		public Scheme[] newArray( int size )
		{
			return new Scheme[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
//		checker = new ICheckKeyValue()
//		{
//			@Override
//			public String checkKey( String key )
//			{
//				String retval = null;
//				try
//				{
//					if ( key.equals( "RecordTypeId" ) )
//					{
//						String id = m_Object.getString( key );
//						SmartStore store = SFApp.GetSmartStore();
//						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {RecordType:Name} from {RecordType} where {RecordType:Id}='" + id + "'", 10 ), 0 );
//						retval = result.getJSONArray( 0 ).getString( 0 );
//					}
//				}
//				catch ( JSONException e )
//				{
//					e.printStackTrace();
//				}
//				return retval;
//			}
//		};
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		
		try
		{
			JSONObject obj = new JSONObject();
			obj.put( "name", "Scheme Benefit Range" );
			obj.put( "id", m_Object.getString( "Id" ) );
			obj.put( "object", new SchemeBenefitRange() );
			arr.put( obj );
			
			obj = new JSONObject();
			obj.put( "name", "Scheme Condition" );
			obj.put( "id", m_Object.getString( "Id" ) );
			obj.put( "object", new SchemeCondition() );
			arr.put( obj );
		}
		catch ( JSONException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return arr;
	}
}
