package com.extentor.sfobjects;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;

public class SFObject implements ISFObject
{
	protected JSONObject		m_Object;
	protected String			m_Data;
	protected Boolean			m_Display;
	protected ArrayList<String>	m_FieldsOption;
	protected String			m_Type, m_Subtype;
	
	protected interface ICheckKeyValue
	{
		public String checkKey(String key);
	}
	
	protected ICheckKeyValue checker = null;

	public SFObject()
	{
		m_Display = false;
		m_FieldsOption = new ArrayList<String>();
	}

	@SuppressWarnings("unchecked")
	public SFObject( Parcel in )
	{
		m_Data = in.readString();
		m_Display = in.readByte() != 0;
		m_FieldsOption = (ArrayList<String>) in.readArrayList( null );
		m_Type = in.readString();
		m_Subtype = in.readString();
		try
		{
			m_Object = new JSONObject( m_Data );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel( Parcel dest, int flags )
	{
		dest.writeString( m_Data );
		dest.writeByte( (byte) ( m_Display ? 1 : 0 ) );
		dest.writeList( m_FieldsOption );
		dest.writeString( m_Type );
		dest.writeString( m_Subtype );
	}

	@Override
	public String getSoupName()
	{
		return m_Type;
	}

	@Override
	public JSONObject getFieldKeyVals()
	{
		return SFObjectHelper.GetFields( m_Type, m_Subtype );
	}

	@Override
	public JSONObject getData()
	{
		return m_Object;
	}

	@Override
	public JSONObject getDetails( )
	{
		JSONObject obj = new JSONObject();
		Iterator<String> iterator = m_Object.keys();
		while ( iterator.hasNext() )
		{
			String key = iterator.next();

			if ( !( this.m_Display ^ m_FieldsOption.contains( key ) ) )
			{
				try
				{
					String checkedVal = null;
					if( checker != null )
					{
						checkedVal = checker.checkKey( key );
					}
					if( checkedVal != null )
					{
						obj.put( key, checkedVal );
					}
					else
					{
						obj.put( key, m_Object.getString( key ) );
					}
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}
			}
		}
		return obj;
	}

	@Override
	public JSONArray getRelated()
	{
		return null;
	}

	@Override
	public void setObject( JSONObject jsonObject )
	{
		m_Data = jsonObject.toString();
	}
}
