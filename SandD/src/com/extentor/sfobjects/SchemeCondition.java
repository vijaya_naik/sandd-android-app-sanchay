package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class SchemeCondition extends SFObject
{
	public SchemeCondition()
	{
		super();

		m_Display = true;// true for display - false for hide
		m_Type = "Scheme_Condition__c";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}

	public SchemeCondition( Parcel in )
	{
		super( in );
		try
		{
			m_Object.put( "parent", "Scheme__c" );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public static final Parcelable.Creator<SchemeCondition>	CREATOR	= new Parcelable.Creator<SchemeCondition>()
	{
		@Override
		public SchemeCondition createFromParcel( Parcel source )
		{
			return new SchemeCondition( source );
		}

		@Override
		public SchemeCondition[] newArray( int size )
		{
			return new SchemeCondition[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
//		checker = new ICheckKeyValue()
//		{
//			@Override
//			public String checkKey( String key )
//			{
//				String retval = null;
//				try
//				{
//					if ( key.equals( "RecordTypeId" ) )
//					{
//						String id = m_Object.getString( key );
//						SmartStore store = SFApp.GetSmartStore();
//						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {RecordType:Name} from {RecordType} where {RecordType:Id}='" + id + "'", 10 ), 0 );
//						retval = result.getJSONArray( 0 ).getString( 0 );
//					}
//				}
//				catch ( JSONException e )
//				{
//					e.printStackTrace();
//				}
//				return retval;
//			}
//		};
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();		
		return arr;
	}
}
