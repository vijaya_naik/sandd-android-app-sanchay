package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.extentor.helper.Helper;
import com.extentor.sfandroid.SFApp;
import com.salesforce.androidsdk.smartstore.store.QuerySpec;
import com.salesforce.androidsdk.smartstore.store.SmartStore;
import android.os.Parcel;
import android.os.Parcelable;

public class Order extends SFObject
{
	public Order()
	{
		super();

		m_Display = true;// true for display - false for hide
		m_Type = "Sales_Order__c";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}

	public Order( Parcel in )
	{
		super( in );
		try
		{
			m_Object.put( "parent", "Buyer__c" );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public static final Parcelable.Creator<Order>	CREATOR	= new Parcelable.Creator<Order>()
	{
		@Override
		public Order createFromParcel( Parcel source )
		{
			return new Order( source );
		}

		@Override
		public Order[] newArray( int size )
		{
			return new Order[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
		checker = new ICheckKeyValue()
		{
			@Override
			public String checkKey( String key )
			{
				String retval = null;
				try
				{
					if ( key.equals( "Price_Book__c" ) )
					{
						String id = m_Object.getString( key );
						SmartStore store = SFApp.GetSmartStore();
						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {Pricebook2:Name} from {Pricebook2} where {Pricebook2:Id}='" + id + "'", 10 ), 0 );
						retval = result.getJSONArray( 0 ).getString( 0 );
					}
					if ( key.equals( "Buyer__c" ) )
					{
						String id = m_Object.getString( key );
						SmartStore store = SFApp.GetSmartStore();
						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {Account:Name} from {Account} where {Account:Id}='" + id + "'", 10 ), 0 );
						retval = result.getJSONArray( 0 ).getString( 0 );
					}
					if ( key.equals( "Date__c" ) )
					{
						String val = m_Object.getString( key );
						retval = Helper.GetDate( val );
					}
				}
				catch ( JSONException e )
				{
					e.printStackTrace();
				}
				return retval;
			}
		};
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		try
		{
			JSONObject obj = new JSONObject();
			obj.put( "name", "Order Line Items" );
			obj.put( "id", m_Object.getString( "Id" ) );
			obj.put( "object", new OrderLineItems() );
			arr.put( obj );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		return arr;
	}
}
