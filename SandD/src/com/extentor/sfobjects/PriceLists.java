package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class PriceLists extends SFObject
{
	public PriceLists()
	{
		super();
		
		m_Display = true;// true for display - false for hide
		m_Type = "Pricebook2";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}
	
	public PriceLists( Parcel in )
	{
		super( in );
	}
	
	public static final Parcelable.Creator<PriceLists> CREATOR = new Parcelable.Creator<PriceLists>()
	{
		@Override
		public PriceLists createFromParcel( Parcel source )
		{
			return new PriceLists( source );
		}

		@Override
		public PriceLists[] newArray( int size )
		{
			return new PriceLists[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		return arr;
	}
}
