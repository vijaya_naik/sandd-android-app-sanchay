package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONObject;
import android.os.Parcelable;

public interface ISFObject extends Parcelable
{
	public String getSoupName();
	
	public JSONObject getFieldKeyVals();

	public JSONObject getData();

	public JSONObject getDetails();

	public JSONArray getRelated();

	public void setObject( JSONObject jsonObject );
}
