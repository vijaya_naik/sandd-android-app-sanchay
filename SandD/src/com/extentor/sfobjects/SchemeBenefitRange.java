package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class SchemeBenefitRange extends SFObject
{
	public SchemeBenefitRange()
	{
		super();

		m_Display = true;// true for display - false for hide
		m_Type = "Scheme_Benefit_Range__c";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}

	public SchemeBenefitRange( Parcel in )
	{
		super( in );
		try
		{
			m_Object.put( "parent", "Scheme__c" );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public static final Parcelable.Creator<SchemeBenefitRange>	CREATOR	= new Parcelable.Creator<SchemeBenefitRange>()
	{
		@Override
		public SchemeBenefitRange createFromParcel( Parcel source )
		{
			return new SchemeBenefitRange( source );
		}

		@Override
		public SchemeBenefitRange[] newArray( int size )
		{
			return new SchemeBenefitRange[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
//		checker = new ICheckKeyValue()
//		{
//			@Override
//			public String checkKey( String key )
//			{
//				String retval = null;
//				try
//				{
//					if ( key.equals( "RecordTypeId" ) )
//					{
//						String id = m_Object.getString( key );
//						SmartStore store = SFApp.GetSmartStore();
//						JSONArray result = store.query( QuerySpec.buildSmartQuerySpec( "select {RecordType:Name} from {RecordType} where {RecordType:Id}='" + id + "'", 10 ), 0 );
//						retval = result.getJSONArray( 0 ).getString( 0 );
//					}
//				}
//				catch ( JSONException e )
//				{
//					e.printStackTrace();
//				}
//				return retval;
//			}
//		};
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		return arr;
	}
}
