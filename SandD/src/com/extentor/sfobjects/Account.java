package com.extentor.sfobjects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Parcel;
import android.os.Parcelable;

public class Account extends SFObject
{
	public Account()
	{
		super();

		m_Display = true;// true for display - false for hide
		m_Type = "Account";
		m_Subtype = null;
		m_FieldsOption = SFObjectHelper.GetFieldOptions( m_Type, m_Subtype );
	}
	
	public Account( Parcel in )
	{
		super( in );
	}
	
	public static final Parcelable.Creator<Account> CREATOR = new Parcelable.Creator<Account>()
	{
		@Override
		public Account createFromParcel( Parcel source )
		{
			return new Account( source );
		}

		@Override
		public Account[] newArray( int size )
		{
			return new Account[size];
		}
	};

	@Override
	public JSONObject getDetails()
	{
		return super.getDetails();
	}

	@Override
	public JSONArray getRelated()
	{
		JSONArray arr = new JSONArray();
		try
		{
			JSONObject obj = new JSONObject();
			obj.put( "name", "Orders" );
			obj.put( "id", m_Object.getString( "Id" ) );
			obj.put( "object", new Order() );
			arr.put( obj );
		}
		catch ( JSONException e )
		{
			e.printStackTrace();
		}
		return arr;
	}
}